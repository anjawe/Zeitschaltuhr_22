#!/bin/bash

echo "C++-Programm wird kompiliert..."

cd Quellcode/

#C++ Dateien compilieren 
g++ -g -fexceptions -I/usr/include/mysql -I/usr/include -I/usr/include/mysql++ -I home/pi/wiringPi -I"/usr/include/mysql++ " -I/usr/include/mysql -I/usr/include -Iinclude -c /home/pi/Zeitschaltuhr_22/Quellcode/DBKommunikation.cpp -o obj/Debug/DBKommunikation.o
g++ -g -fexceptions -I/usr/include/mysql -I/usr/include -I/usr/include/mysql++ -I home/pi/wiringPi -I"/usr/include/mysql++ " -I/usr/include/mysql -I/usr/include -Iinclude -c /home/pi/Zeitschaltuhr_22/Quellcode/main.cpp -o obj/Debug/main.o
g++ -g -fexceptions -I/usr/include/mysql -I/usr/include -I/usr/include/mysql++ -I home/pi/wiringPi -I"/usr/include/mysql++ " -I/usr/include/mysql -I/usr/include -Iinclude -c /home/pi/Zeitschaltuhr_22/Quellcode/Systemuhr.cpp -o obj/Debug/Systemuhr.o
g++ -g -fexceptions -I/usr/include/mysql -I/usr/include -I/usr/include/mysql++ -I home/pi/wiringPi -I"/usr/include/mysql++ " -I/usr/include/mysql -I/usr/include -Iinclude -c /home/pi/Zeitschaltuhr_22/Quellcode/Urlaubsmodus.cpp -o obj/Debug/Urlaubsmodus.o
g++ -g -fexceptions -I/usr/include/mysql -I/usr/include -I/usr/include/mysql++ -I home/pi/wiringPi -I"/usr/include/mysql++ " -I/usr/include/mysql -I/usr/include -Iinclude -c /home/pi/Zeitschaltuhr_22/Quellcode/Verbraucher.cpp -o obj/Debug/Verbraucher.o
g++ -L/usr/lib -L../../../../../wiringPi -L/usr/lib -o bin/Debug/ZSU_22 obj/Debug/DBKommunikation.o obj/Debug/main.o obj/Debug/Systemuhr.o obj/Debug/Urlaubsmodus.o obj/Debug/Verbraucher.o   /usr/lib/libmysqlpp.so /usr/lib/libmysqlclient.so.16 ../../../../../../../usr/lib/libwiringPi.so
