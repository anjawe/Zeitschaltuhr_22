# Mysql im Terminal starten
# mysql -u root -p --local-infile

#DB erstellen
create database zeitschaltuhr;

# enthält den aktuellen zustand der Verbraucher
create table verbraucherzustaende (verbraucher int primary key, zustand int );
insert into verbraucherzustaende (verbraucher, zustand) values ('1','0');
insert into verbraucherzustaende (verbraucher, zustand) values ('2','0');

# enthält nur einen eintrag, der status ist entweder 0 (urlaubsmodus ist aus) oder 1 (urlaubsmodus ist an)
create table urlaubsmodus ( status int primary key );
insert into urlaubsmodus ( status ) values ('0');

#enthält zwei Eintraege Modus Taster1 und Modus Taster2
create table tastermodus ( verbraucher int primary key, modus int );
insert into tastermodus ( verbraucher, modus ) values ('1','1');
insert into tastermodus ( verbraucher, modus ) values ('2','1');

#enthält geplannte schaltzeitpunkte
create table schaltzeiten ( id int primary key, verbraucher int, hh_an int, mm_an int, hh_aus int, mm_aus int, Mo int, Di int, Mi int, Do int, Fr int, Sa int, So int );
	# id: id 1 und 2 gehören zu verbraucher 1, 3 und 4 zu verbraucher 2, usw...
	# Mo bis So: ist entweder 0 wenn die eingetragenen Schaltzeiten inaktiv sind oder 1 wenn sie für diesen Tag aktiv sind.
	# hh_an: Stunde von 0-23 in der eingeschaltet wird
	# mm_an: Minute von 0-59 in der eingeschaltet wird
	# hh_aus: Stunde von 0-23 in der ausgeschaltet wird
	# mm_aus: Minute von 0-59 in der ausgeschaltet wird

# Schaltzeit einfügen
insert into schaltzeiten ( id, verbraucher, hh_an, mm_an, hh_aus, mm_aus, Mo, Di, Mi, Do, Fr, Sa, So ) values ('1', '1', '22', '59', '13', '45', '1', '1', '1', '1', '1', '1', '1');
# Schaltzeit beschreiben bzw. ändern
update schaltzeiten
set hh_an='8', mm_an='15', hh_aus='13', mm_aus='30', Mo='1', Di='1', Mi='1', Do='1', Fr='1', Sa='1', So='1'
where ( id = '1' );

#enthält vergangene Aenderungen der Verbraucher
create table schalthistorie ( id int primary key auto_increment, verbraucher int, ereignis int, zeitpunkt TIMESTAMP DEFAULT CURRENT_TIMESTAMP);
	# id: hat keine bedeutung
	# ereignis: 0 = verbraucher wurde ausgeschaltet 1 = verbraucher wurde eingeschaltet 
	# zeitpunkt: speichert den aktuellen Zeitpunkt (Datum und Uhrzeit)


#Wartung, so dass die historie immer vierzehn Tage alt ist
delete from schalthistorie where DATEDIFF(NOW(), zeitpunkt) >= 14;



# zähle die anzahl der einträge bzw. ids
select count(id) from schalthistorie;


# Einträge erstellen
insert into schalthistorie (verbraucher, ereignis ) values ( 1, 1);


# Eine Schaltzeit löschen
delete from schaltzeiten where id = 1;


##alter table schaltzeiten add column (xxx int);

# ganze Tabelle anzeigen
select * from schaltzeiten;

