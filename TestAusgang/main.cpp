#include <iostream>
#include <wiringPi.h>

using namespace std;

int main ()

{
   //Wiring Pi Setup prüfen
    if( wiringPiSetup() == -1)
    {
       cout << "Fehler Pi!";
       return 1;

    }

    //Pinfestlegung
    const int ledV1 = 3;  //Real: Pin=15 GPIO 22 ; Verbraucher 1
    const int ledV2 = 4; //Real: Pin=16 GPIO 23 ; Verbraucher 2
    const int ledT1 = 28; //Real: Pin= 38 GPIO 20 ; Taster 1
    const int ledT2 = 29;  //Real: Pin= 40 GPIO 21 ; Taster 2


    // Pins als Ausgang oder Eingang deklarieren
    //Verbraucher 1 = Ausgang
    pinMode(ledV1, OUTPUT);

    //Verbraucher 2 = Ausgang
    pinMode(ledV2, OUTPUT);

    //Taster 1 als Eingang
    pinMode(ledT1, INPUT);

    //Taster 2 als Eingang
    pinMode(ledT2, INPUT);

    int eingangTaster1 = 0;
    int eingangTaster2 = 0;

    int led1 = 0;
    int led2 = 0;

    while (1)
    {
        eingangTaster1 = digitalRead(ledT1);

        cout << "Taster 1: " << eingangTaster1 <<endl;

        eingangTaster2 = digitalRead(ledT2);

        cout << "Taster 2: " << eingangTaster2 <<endl;

        led1 = digitalRead(ledV1);

        cout << "Verbraucher1: " << led1<<endl;

        led2 = digitalRead(ledV2);

        cout << "Verbraucher 2: " << led2 <<endl;

        delay (1000);

    }
    return 0;
}
