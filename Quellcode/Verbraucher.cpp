/** \brief Dateiinformationen Verbraucher.cpp

Projekt:	Zeitschaltuhr
Hersteller:	Timecode 22 GmbH

Erstellt von:	@authr Anja Wenk
Datum:		    2015.12.08
Revision:	    Raphael Kuttruf
Datum:		    2016.03.02

cpp-Datei der Klasse Verbraucher
*/

#include "Verbraucher.h"

using namespace std;
//
// Methods
//


// Accessor methods
//


// Other methods
//
/**
 * Konstruktor der Klasse Verbraucher
 */
Verbraucher::Verbraucher()
{

}


/**
 * Spezieller Konstruktor der Klasse Verbraucher
 * @param  Zuordnung gibt die Zuordnung zu welchem Objekt es gehört zb. 1 steht für
 * Verbraucher 1
 * @param Portzuordnung ordnet dem Verbraucher einen Pin zu
 * @param PinTaster orndet dem Taster einen Pin zu der ausgelesen werden soll
 */
Verbraucher::Verbraucher (int Zuordnung, int Pinzuordnung, int PinTaster) : zuordnung(Zuordnung), pin(Pinzuordnung), pinTaster(PinTaster)
{
    zuordnung = Zuordnung;
    pin = Pinzuordnung;
    pinTaster = PinTaster;
    verbraucherStatus = 0;  //Startinitalisierung
    mod4Merker = 0;         //Startinitalisierung

}


/**
 * Verbindet das Objekt der Klasse DBKommunikation mit der Klasse Verbraucher um
 * Eigenschaften zu übernehmen-> Komposition
 * @param  obj Setzt das Objekt der Klasse DBKommunikation
 */
void Verbraucher::SetzeDB(const DBKommunikation &objekt)
{
    dbkom = objekt;
}


/**
 * Holt aus Datenbank nächste Schaltzeit in 5 Minuten
 */
void Verbraucher::HoleSchaltzeit()
{
    int minuten = uhr.AktMin();
    int stunden = uhr.AktStd();
    int wochentag = uhr.AktTagWoche();

    Umrechnung(1,5,minuten,stunden, wochentag); //Addiert 5 Minuten zu der aktuellen Minutenanzahl
    if(mod4Merker == 0)
    {
        dbkom.AktualisiereDB(stunden,minuten,wochentag); // Aktualisiert DB und sucht nach Schaltzeit in 5 Minuten
    }
    PruefeUrlaubaktiv();
}


/**
 * Aktualisiert die Zeit und löscht einmal am Tag die Einträge der Schalthistorie
 * des Datumstages am folgenden Tag aus dem letzten Monats und berechnet die
 * Änderung durch den Urlaubsmodus neu
 */
void Verbraucher::AktZeit()
{
    uhr.AktZeit();

    int b = uhr.AktMin();
    int a = uhr.AktStd();

    if ( a == 22 && b == 59 ) //Tägliche Änderungen
    {
        AenderungBerechnen(); //Berechnet die Änderungen für den Urlaubsmodus
        dbkom.LoescheEintraege(); //Löscht die Einträge aus dem letzten Monat
    }
}


/**
 * Fordert die Klasse Urlaubsmodus dazu auf die Änderungen durch den aktiven
 * Urlaubsmodus zu berechnen und ob dieser Wert subtrahiert oder addiert werden
 * soll
 */
void Verbraucher::AenderungBerechnen()
{
    urlaub.BerechAenderung(); //Änderung des Zeitpunktes zwischen 1 und 5 Minuten
    urlaub.Zufallssumme(); //Zufällig bestimmt ob Änderung addiert oder subtrahiert wird
    return;
}


/**
 * Vergleicht ob Schaltzeitpunkt in Minuten und aktueller Zeitpunkt in Minuten
 * gleich sind und ob die Stunden gleich sind
 * @param  SchaltMinuten Schaltzeitpunkt Minutenangabe bei der geschalten werden
 * soll
 * @param  SchaltStunden gibt Zeitstunde der Schaltzeit an.
 */
void Verbraucher::VglZeiten (int SchaltMinuten, int SchaltStunden)
{
    uhr.AktZeit();
    int c = uhr.AktMin();
    int d = uhr.AktStd();
    int e = Mod4Merker();


    if (( SchaltMinuten == c ) && ( SchaltStunden == d )) //Prüfen, ob der Schaltzeitpunkt erreicht wurde

    {
        if ( e == 1 ) //Sollte der Timer aus Tastermodus 4 aktiv sein
        {
            int g = dbkom.Id(); //Wiedererkennung Modus 4 und löschen des Eintrages in der Tabelle Schaltzeiten


            if(g == 101 || g == 102) //Wiedererkennung Modus 4 abschalten und löschen, wenn Schaltzeit gleich Timereintrag
            {
                dbkom.Modus4Loeschen();
                SetzeMod4Merker(0);
                SetzteVerbraucher(0);
            }
            else
            {
                return;
            }
        }

        else //Timer nicht aktiv, Schaltzeit kann wirken
        {

            int b = digitalRead(pinTaster);  //aktueller Tastereingan

            if( b == 0 )
            {
                int gepl = dbkom.Verbraucherzustand(); //geplanter Zustand des Vebrauchers

                if ( gepl == 0 )
                {
                    SetzteVerbraucher(0); //Verbraucher wird ausgeschalten
                }

                if ( gepl == 1 )
                {
                    SetzteVerbraucher(1); //Verbraucher wird eingeschalten
                }
            }

            if ( b == 1 )
            {
                Tastermodus();
            }
        }
    }
    return;
}


/**
 * Prüft ob Urlaubsmodus aktiv ist aus der Datenbank und ruft vglZeiten auf je nach Änderung der Zeit durch den Urlaubsmodus
 */
void Verbraucher::PruefeUrlaubaktiv ()
{
    int u = dbkom.Urlaubsmoduszustand();
    int stundenAnzahl;
    int minutenAnzahl;
    int wochentag;

    stundenAnzahl = dbkom.Schaltstunde();
    minutenAnzahl = dbkom.Schaltminute();
    wochentag = dbkom.SchaltWoTag();

    if ( u == 0 ) //Urlaubsmodus ist inaktiv
    {
        VglZeiten(minutenAnzahl, stundenAnzahl);
    }

    if( u == 1 ) //Urlaubsmodus aktiv und Änderung wird auf Schaltzeit addiert oder subrahiert
    {
        int mod4 = Mod4Merker();

        if(mod4 == 1)
        {
            VglZeiten(minutenAnzahl,stundenAnzahl);
        }
        if(mod4 == 0)
        {
            int aenderungZeit;
            int zufall;

            aenderungZeit = urlaub.Aenderung();

            zufall = urlaub.SuboderAdd();

            Umrechnung(zufall,aenderungZeit,minutenAnzahl,stundenAnzahl,wochentag);

            VglZeiten(minutenAnzahl, stundenAnzahl);
        }
    }
    return;
}


/**
 * Schaltet Verbraucher an oder aus, wenn der aktuelle und der geplante Zustand
 * sich unterscheiden und leitet weiter zu ddem Schalthistorieneintrag und die
 * Verbrauchertabelle in der Datenbank
 * @param  GeplZustand Setzte den Zustand des Verbrauchers aus 1 oder 0.
 */
void Verbraucher::SetzteVerbraucher (int GeplanterZustand)
{
    if ( verbraucherStatus == GeplanterZustand ) //Sollte sich der Zustand nicht ändern gibt es keinen Schalthistorieneintrag
    {
        return;
    }
    else
    {
        verbraucherStatus = GeplanterZustand;

        //Eintrag in Schalthistorie
        dbkom.SchalthistorienEintrag(zuordnung, verbraucherStatus); //Zustandsänderung wird an Schalthistorie übergeben
        dbkom.Verbraucherzustaende(verbraucherStatus);

        //Setzen des Raspberry Pis Ausgangs und der LED

        digitalWrite(pin,verbraucherStatus);

        //Sprachausgabe des Ergebnisses
        Sprachausgabe();

        //cout << "Verbraucher hat geschalten" << endl;
    }
}


/**
 * Gibt den aktuellen Zustand des Verbrauchers aus
 * @return int
 */
int Verbraucher::AktZustandVerbraucher()
{
    return verbraucherStatus;
}


/**
 * Durch den eingestellen Tastermodus in der Datenbank springt die Funktion die
 * jeweiligen Tastereigenschaft und schaltet dementsprechend den Verbraucher
 */
void Verbraucher::Tastermodus ()
{
    int modus = dbkom.HoleTastMod();

    switch ( modus )
    {
    case 1: //Deaktivierter Modus Verbraucherzustand ändert sich nicht
    {
        break;
    }

    case 2: //Haltemodus
    {
        SetzteVerbraucher(1);
        while( digitalRead(pinTaster)==1); //Solange der Tastergedrückt wird bleibt der Verbraucher an

        SetzteVerbraucher(0);
        break;
    }
    case 3: //Tastermodus
    {
        int a = AktZustandVerbraucher();

        if ( a == 0 ) //Ist der Verbraucher aktuell aus wird er eingeschalten
        {
            SetzteVerbraucher(1);
        }

        if( a == 1 )
        {
            SetzteVerbraucher(0);
        }
        break;
    }

    case 4: //Timermodus
    {
        SetzteVerbraucher(1);

        int mod4 = Mod4Merker();
        if(mod4 == 1) //Timer immernoch aktiv
        {
            return;
        }

        else //Timer nicht aktiv -> neuer Timermodus einstellbar
        {
            AktZeit();
            int minute = uhr.AktMin();
            int stunde = uhr.AktStd();
            int wotag = uhr.AktTagWoche();
            int id = 0;


            Umrechnung(1,5,minute,stunde,wotag); //Endzeit des Timers wird berechnet aktueller Zeitpunkt+5Min;
            if( zuordnung ==1 )
            {
                id = 101;
            }
            if(zuordnung == 2)
            {
                id = 102;
            }
            dbkom.Modus4Hinzu(id,zuordnung, stunde, minute, wotag); //Ausschaltzeit wird in Schalthistorie hinzugefügt
            dbkom.AktualisiereDB(stunde,minute,wotag); //Aktualisert DB
            SetzeMod4Merker(1);
        }
        break;
    }
    }
    return;
}



/**
 * Berechnet den neuen Zeitpunkt der Minuten, Stunden und des Wochentages z.B.
 * Aktivierung des Urlaubsmoduses
 * @param  Suboderadd Gibt an ob der Wert der Änderung von den Minuten abgezogen
 * oder addiert wird
 * @param  Wert Wert um den sich die Minutenanzahl verändern soll
 * @param  Minute Minutenanzahl, die verändert werden soll* @param  Stunde Stundenanzahl, die im Fall einer Überschreitung ( >59 Min) oder
 * Unterschreitung ( <0 Min) neu berechnet werden soll
 * @param  Woche Wochentag, der im Fall einer Überschreitung (>23 Stunden) und
 * Unterschreitung (<0) neu berechnet werden soll
 */
void Verbraucher::Umrechnung(int Suboderadd, int Wert, int &Minute, int &Stunde, int &Woche)
{
    if( Suboderadd == 1 )   //Addition des Wertes
    {
        Minute = Minute + Wert;

        if (Minute > 59)
        {
            Minute = Minute - 60;
            Stunde = Stunde + 1;
            Woche = Woche;

            if (Stunde > 23)
            {
                Stunde = Stunde -24;
                Woche = Woche + 1;

                if(Woche > 6)
                {
                    Woche = Woche - 7;
                }
            }
        }
    }

    if( Suboderadd == 2 )   //Subtraktion des Wertes
    {
        Minute = Minute-Wert;
        if (Minute < 0)
        {
            Minute = Minute + 60;
            Stunde = Stunde - 1;

            if (Stunde < 0)
            {
                Stunde = Stunde + 24;
                Woche = Woche - 1;

                if(Woche < 0)
                {
                    Woche = Woche + 7;
                }
            }
        }
    }
}


/**
 * Setzt den Modus 4 Merker auf 1, wenn der Timer aktiv ist.
 * @param  Mod4Zustand Setzt den Modus 4 Merker auf den gewünschten Zustand; 1=
 * Timer aktiv
 */
void Verbraucher::SetzeMod4Merker (int Mod4Zustand)
{
    mod4Merker = Mod4Zustand;
}


/**
 * Gibt aus, ob der Timer noch aktiv ist
 * @return int
 */
int Verbraucher::Mod4Merker ()
{
    return mod4Merker;
}

/**
* Gibt beim An- und Ausschalten eines Verbraucher eine Sprachausgabe aus
*/
void Verbraucher::Sprachausgabe ()
{
    string zustand;
   //Datei anlegen
    system("mkdir audio");
    ofstream ausgabe ("audio//audio.sh");

    if (!ausgabe)
    {
        cout << "Datei konnte nicht geöffnet werden";
    }

    if(verbraucherStatus == 0)
    {
        zustand = "ausgeschaltet.";
    }
    else
    {
        zustand = "eingeschaltet.";
    }

    ausgabe << "#!/bin/bash\n"
            << "espeak -v  mb-de5 -s 140  "
            << "'Verbraucher " << zuordnung
            << " wird " << zustand << "'" << " 2>/dev/null"
            << endl;
    ausgabe.close();
    system("chmod +x ./audio/audio.sh");
    system("./audio/audio.sh");

}

/**
* Fernzugriff/Fernsteuerung mittels App über eine SSH Verbindung
*/
void Verbraucher::Fernzugriff ()
{
    int a = AktZustandVerbraucher();
    if (a == 1)
        SetzteVerbraucher(0);
    if (a == 0)
        SetzteVerbraucher(1);
}
