/** \brief Dateiinformationen Systemuhr.h

Projekt:	Zeitschaltuhr
Hersteller:	Timecode 22 GmbH

Erstellt von:	@authr Anja Wenk
Datum:		    2015.12.08
Revision:	    Raphael Kuttruf
Datum:		    2016.03.02

Headerdatei der Klasse Systemuhr
*/

#ifndef SYSTEMUHR_H
#define SYSTEMUHR_H

#include <time.h>
#include "iostream"

class Systemuhr
{

public:


    /**
         * Konstruktor der Klasse Systemuhr
         */
    Systemuhr ();


    /**
     * Gibt die aktuelle Uhrzeit, Datumstag und den Wochentag aus.
     */
    void AktZeit ();


    /**
     * Gibt aktuelle Minuten zurück
     * @return int
     */
    int AktMin ();


    /**
     * Gibt aktuelle Stunde zurück
     * @return int
     */
    int AktStd ();


    /**
     * Gibt aktuellen Tag im Monat zurück (Datumstag)
     * @return int
     */
    int AktTagMon ();


    /**
     * Gibt aktuellen Wochentag zurück
     * @return int
     */
    int AktTagWoche ();


    /**
     * Gibt aktuelle Sekunden zurück
     * @return int
     */
    int AktSek ();


    /**
     * Gibt aktuellen Monat zurück
     * @return int
     */
    int AktMonat ();

protected:

public:

protected:

public:

protected:


private:

    // Private attributes
    //

    // Gibt die aktuelle Anzahl der Stunden der Uhrzeit zu diesem Zeitpunkt aus.
    int aktStunden;

    // Gibt die aktuelle Anzahl der Minuten zu dieser Uhrzeit zu diesem Zeitpunkt aus.
    int aktMinuten;

    // Gibt aktuellen Monatstag aus (1-31)
    int aktTag;

    // Aktueller Wochentag 0=Sonntag 1=Montag
    int aktWoTag;

    // aktuelle Sekunden (0-59)
    int aktSekunden;

    // aktueller Monat (1-12)
    int aktMon;


public:

private:

public:

private:



};

#endif // SYSTEMUHR_H
