/** \brief Dateiinformationen Verbraucher.h

Projekt:	Zeitschaltuhr
Hersteller:	Timecode 22 GmbH

Erstellt von:	@authr Anja Wenk
Datum:		    2015.12.08
Revision:	    Raphael Kuttruf
Datum:		    2016.03.02

Headerdatei der Klasse Verbraucher
*/

#ifndef VERBRAUCHER_H
#define VERBRAUCHER_H

#include "DBKommunikation.h"
#include "Urlaubsmodus.h"
#include "Systemuhr.h"

#include <wiringPi.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <fstream>
#include "iostream"
#include <string.h>

class Verbraucher
{
public:

    /**
     * Konstruktor der Klasse Verbraucher
     */
    Verbraucher ();


    /**
     * Spezieller Konstruktor der Klasse Verbraucher
     * @param  Zuordnung gibt die Zuordnung zu welchem Objekt es gehört zb. 1 steht für
     * Verbraucher 1
     * @param Portzuordnung ordnet dem Verbraucher einen Pin zu
     * @param PinTaster orndet dem Taster einen Pin zu der ausgelesen werden soll
     */
    Verbraucher ( int Zuordnung, int Pinzuordnung, int PinTaster);


    /**
     * Holt aus Datenbank nächste Schaltzeit in 5 Minuten
     */
    void HoleSchaltzeit ();

    /**
     * Aktualisiert die Zeit und löscht einmal am Tag die Einträge der Schalthistorie
     * des Datumstages am folgenden Tag aus dem letzten Monats und berechnet die
     * Änderung durch den Urlaubsmodus neu
     */
    void AktZeit ();


    /**
    * Schaltet Verbraucher an oder aus, wenn der aktuelle und der geplante Zustand
    * sich unterscheiden und leitet weiter zu ddem Schalthistorieneintrag und die
    * Verbrauchertabelle in der Datenbank
    * @param  GeplZustand Setzte den Zustand des Verbrauchers aus 1 oder 0.
    */
    void SetzteVerbraucher (int GeplZustand);


    /**
     * Durch den eingestellen Tastermodus in der Datenbank springt die Funktion die
     * jeweiligen Tastereigenschaft und schaltet dementsprechend den Verbraucher
     */
    void Tastermodus ();


    /**
     * Prüft den Tasteingang und gibt den aktuellen Zustand zurück
     * @return int
     */
    int PruefeTastereingang ();

    /**
     * Verbindet das Objekt der Klasse DBKommunikation mit der Klasse Verbraucher um
     * Eigenschaften zu übernehmen-> Komposition
     * @param  obj Setzt das Objekt der Klasse DBKommunikation
     */
    void SetzeDB (const DBKommunikation &objekt);


    /**
    * Fordert die Klasse Urlaubsmodus dazu auf die Änderungen durch den aktiven
    * Urlaubsmodus zu berechnen und ob dieser Wert subtrahiert oder addiert werden
    * soll
    */
    void AenderungBerechnen ();

    /**
    * Gibt beim An- und Ausschalten eines Verbraucher eine Sprachausgabe aus
    */
    void Sprachausgabe ();

    /**
    * Fernzugriff/Fernsteuerung mittels App über eine SSH Verbindung
    */
    void Fernzugriff ();


protected:

public:

protected:

public:

protected:


private:

    // Private attributes
    //

    // Zustand Verbraucher
    int verbraucherStatus;

    // Zuordnung zu welchem Verbraucher das Objekt gehört
    int zuordnung;

    // Aktueller Zustand des Tasters; 1= Taster wird nach unten gedrückt und gehalten
    int tasterMerker;


    // Objekt der Klasse DBKommunikation
    DBKommunikation dbkom;

    // Objekt der Klasse Systemuhr
    Systemuhr uhr;

    // Objekt der Klasse Urlaubsmodus
    Urlaubsmodus urlaub;

    // Gibt an, ob der Timer noch aktiv ist
    int mod4Merker;

    //Ordnet dem Verbraucher einen Pin zu
    int pin;

    //Tasterpin
    int pinTaster;


public:

private:

    /**
     * Vergleicht ob Schaltzeitpunkt in Minuten und aktueller Zeitpunkt in Minuten
     * gleich sind und ob die Stunden gleich sind
     * @param  SchaltMinuten Schaltzeitpunkt Minutenangabe bei der geschalten werden
     * soll
     * @param  SchaltStunden gibt Zeitstunde der Schaltzeit an.
     */
    void VglZeiten (int SchaltMinuten, int SchaltStunden);


    /**
     * Prüft ob Urlaubsmodus aktiv ist in UI
     */
    void PruefeUrlaubaktiv ();


    /**
     * Gibt den aktuellen Zustand des Verbrauchers aus
     * @return int
     */
    int AktZustandVerbraucher ();


    /**
     * Berechnet den neuen Zeitpunkt der Minuten, Stunden und des Wochentages z.B.
     * Aktivierung des Urlaubsmoduses
     * @param  Suboderadd Gibt an ob der Wert der Änderung von den Minuten abgezogen
     * oder addiert wird
     * @param  Wert Wert um den sich die Minutenanzahl verändern soll
     * @param  Minute Minutenanzahl, die verändert werden soll
     * @param  Stunde Stundenanzahl, die im Fall einer Überschreitung ( >59 Min) oder
     * Unterschreitung ( <0 Min) neu berechnet werden soll
     * @param  Woche Wochentag, der im Fall einer Überschreitung (>23 Stunden) und
     * Unterschreitung (<0) neu berechnet werden soll
     */
    void Umrechnung (int Suboderadd, int Wert, int &Minute, int &Stunde, int &Woche);


    /**
     * Setzt den Modus 4 Merker auf 1, wenn der Timer aktiv ist.
     * @param  Mod4Zustand Setzt den Modus 4 Merker auf den gewünschten Zustand; 1=
     * Timer aktiv
     */
    void SetzeMod4Merker (int Mod4Zustand);


    /**
     * Gibt aus, ob der Timer noch aktiv ist
     * @return int
     */
    int Mod4Merker ();

public:

private:



};

#endif // VERBRAUCHER_H
