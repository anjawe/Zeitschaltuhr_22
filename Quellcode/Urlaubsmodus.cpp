/** \brief Dateiinformationen Urlaubsmodus.cpp

Projekt:	Zeitschaltuhr
Hersteller:	Timecode 22 GmbH

Erstellt von:	@authr Anja Wenk
Datum:		    2015.12.08
Revision:	    Raphael Kuttruf
Datum:		    2016.03.02

cpp-Datei der Klasse Urlaubsmodus
*/
#include "Urlaubsmodus.h"

//
// Methods
//


// Accessor methods
//


// Other methods
//


/**
 * Konstruktor des Urlaubsmoduses
 */
Urlaubsmodus::Urlaubsmodus ()
{

}


/**
* Berechnet zufällig den Wert um den die Schaltzeit verändert werden soll
*/
void Urlaubsmodus::BerechAenderung ()
{

    srand(time(NULL));
    aendZeit = rand() % 5 + 1;

}


/**
* Gibt die zufällige Verzögerung der Schaltzeit zwischen einer und zehn Minuten aus
* @return int
*/
int Urlaubsmodus::Aenderung ()
{
    return aendZeit;
}


/**
* Berechnet zufällig, ob die Änderung subtrahiert oder addiert wird zur Schaltzeit
*/
void Urlaubsmodus::Zufallssumme ()
{

    srand(time(NULL));
    subadd = rand() % 2 +1;
}


/**
* Gibt zurück ob die Änderung nun subrahiert oder addiert wird
* @return int
*/
int Urlaubsmodus::SuboderAdd()
{
    return subadd;
}

