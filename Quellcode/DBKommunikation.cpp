/** \brief Dateiinformationen DBKommunikation.cpp

Projekt:	Zeitschaltuhr
Hersteller:	Timecode 22 GmbH

Erstellt von:	@authr Anja Wenk
Datum:		    2015.12.08
Revision:	    Raphael Kuttruf
Datum:		    2016.03.02

cpp-Datei der Klasse DBKommunikation
*/

#include "DBKommunikation.h"


//
// Methods
//


// Accessor methods
//


// Other methods
//
/**
 * Konstruktor der Klasse DBKommunikation
 */
DBKommunikation::DBKommunikation()
{

}


/**
 * Spezieller Konstruktor
 * @param  Zuordnung Zuordnung zu welchem Objekt gehörig
 * @param  User Speichert den User der Datenbank ab
 * @param  Passwd Speichert das Passwort des Users
 */
DBKommunikation::DBKommunikation (int Zuordnung, const string& User, const string& Passwd) : zuordnungdb(Zuordnung), conn( false )
{
    zuordnungdb = Zuordnung;
    ready = conn.connect( "zeitschaltuhr", "localhost", User.c_str(), Passwd.c_str() );
}


/**
 * Schaltet den Verbraucher im Falle eines Stromausfalls etc auf den letzten
 * gespeicherten Zustand, der in der Datenbank gespeichert ist
 */
void DBKommunikation::StartZustandVerb()
{
    mysqlpp::Query query = conn.query();

    //SQL Abfrage
    query   << " SELECT * "
            << " FROM verbraucherzustaende "
            << " WHERE verbraucher =" << mysqlpp::quote << zuordnungdb;

    //store-Funktion zur sql-Abfrage
    mysqlpp::StoreQueryResult r = query.store();
    zustandVerb = (int(r[0]["zustand"]));
}


/**
 * Trägt die Verbraucherzustandsänderung in die Datenbank ein
 * @param  VerbraucherZuordnung Gibt an um welchen Verbraucher es sich handelt
 * @param  VerbraucherZust Gibt den Zustand an, wie sich der Verbraucher geändert
 * hat; 1= Verbraucher wurde angeschalten
 */
void DBKommunikation::SchalthistorienEintrag (int VerbraucherZuordnung, int VerbraucherZust)
{
    //cout << "in Schalthistorieneintrag" <<endl;

    //Query präparieren
    mysqlpp::Query query = conn.query();
    query << "insert into schalthistorie ( verbraucher, ereignis) values "
            << "( '" << mysqlpp::quote << VerbraucherZuordnung
            << "', '" << mysqlpp::quote << VerbraucherZust
            << "')";

            query.execute();
            return;
}


/**
* Aktualisiert die Variablen der Klasse DBKommunikation für den nächsten
* Schaltzeitpunkt in 5 Minuten
* @param  Stunde Suche nach Stunde, in der Verbraucher schalten soll nach
* Schaltplan
* @param  Minute Suche nach Minute in der sich der Verbraucher ändern soll laut
* Schaltplan
* @param  WoTag Wochentag nachdem in Datenbank gesucht werden soll
*/
void DBKommunikation::AktualisiereDB(int Stunde, int Minute, int Wotag)
{
    switch (Wotag)
    {
    case 0: //aktueller Wochentag ist Sonntag
    {
        mysqlpp::Query query = conn.query();

        //SQL Abfrage
        query   << " SELECT * "
                << " FROM schaltzeiten "
                << " WHERE mm_an =" << mysqlpp::quote << Minute
                << " AND hh_an = " << mysqlpp::quote << Stunde
                << " AND So = '1' "
                << " AND verbraucher = " << mysqlpp::quote << zuordnungdb;

        //store-Funktion zur sql-Abfrage
        mysqlpp::StoreQueryResult r = query.store();

        //Überprüfung ob sql-Abfrage ergebnis zurück gibt sollte dies nicht der Fall sein wird geprüft, ob Ausschaltzeit vorhanden
            if( r.num_rows() == 0 )
            {
                //Query präparieren
                mysqlpp::Query query = conn.query();

                //SQL Abfrage
                query   << " SELECT * "
                        << " FROM schaltzeiten "
                        << " WHERE mm_aus =" << mysqlpp::quote << Minute
                        << " AND hh_aus = " << mysqlpp::quote << Stunde
                        << " AND So = '1'"
                        << " AND verbraucher = " << mysqlpp::quote << zuordnungdb;

                 //store-Funktion zur sql-Abfrage
                mysqlpp::StoreQueryResult r = query.store();

                    //Überprüfung ob Ausschaltzeit vorhanden
                    if( r.num_rows() == 0 )
                    {
                        //cout << "Keinerlei Übereinstimmung" << endl;
                        return;
                    }
                    else
                    {
                         //Setzung der Variablen
                        id = (int(r[0]["id"]));
                        verbZuordnung = (int(r[0]["verbraucher"]));
                        schaltStd = (int(r[0]["hh_aus"]));
                        schaltMin = (int(r[0]["mm_aus"]));
                        zustandVerb = 0;
                        schaltWoTag = 0;
                        break;
                    }
            }
        else
        {
            id = (int(r[0]["id"]));
            verbZuordnung = (int(r[0]["verbraucher"]));
            schaltStd = (int(r[0]["hh_an"]));
            schaltMin = (int(r[0]["mm_an"]));
            zustandVerb = 1;
            schaltWoTag = 0;
            break;
        }
    }

    case 1: //Aktueller Wochentag ist ein Montag
    {
        mysqlpp::Query query = conn.query();

        //SQL Abfrage
        query   << " SELECT * "
                << " FROM schaltzeiten "
                << " WHERE mm_an =" << mysqlpp::quote << Minute
                << " AND hh_an = " << mysqlpp::quote << Stunde
                << " AND Mo = '1' "
                << " AND verbraucher = " << mysqlpp::quote << zuordnungdb;

        //store-Funktion zur sql-Abfrage
        mysqlpp::StoreQueryResult r = query.store();


        //Überprüfung ob sql-Abfrage ergebnis zurück gibt sollte dies nicht der Fall sein wird geprüft, ob Ausschaltzeit vorhanden
        if( r.num_rows() == 0 )
        {
            //Query präparieren
            mysqlpp::Query query = conn.query();

            //SQL Abfrage
            query   << " SELECT * "
                    << " FROM schaltzeiten "
                    << " WHERE mm_aus =" << mysqlpp::quote << Minute
                    << " AND hh_aus = " << mysqlpp::quote << Stunde
                    << " AND Mo = '1'"
                    << " AND verbraucher = " << mysqlpp::quote << zuordnungdb;

            //store-Funktion zur sql-Abfrage
            mysqlpp::StoreQueryResult r = query.store();

            //Überprüfung ob Ausschaltzeit vorhanden
            if( r.num_rows() == 0)
            {
                //cout << "Keinerlei Übereinstimmung" << endl;

                return;
            }
            else
            {
                        //Setzung der Variablen
                        id = (int(r[0]["id"]));
                        verbZuordnung = (int(r[0]["verbraucher"]));
                        schaltStd = (int(r[0]["hh_aus"]));
                        schaltMin = (int(r[0]["mm_aus"]));
                        zustandVerb = 0;
                        schaltWoTag = 1;
                        break;
            }
        }

        else
        {
            id = (int(r[0]["id"]));
            verbZuordnung = (int(r[0]["verbraucher"]));
            schaltStd = (int(r[0]["hh_an"]));
            schaltMin = (int(r[0]["mm_an"]));
            zustandVerb = 1;
            schaltWoTag = 1;
            break;
        }
    }

    case 2: //Aktueller Wochentag ist ein Dienstag = 2
    {
        mysqlpp::Query query = conn.query();

        //SQL Abfrage
        query   << " SELECT * "
                << " FROM schaltzeiten "
                << " WHERE mm_an =" << mysqlpp::quote << Minute
                << " AND hh_an = " << mysqlpp::quote << Stunde
                << " AND Di = '1' "
                << " AND verbraucher = " << mysqlpp::quote << zuordnungdb;

        //store-Funktion zur sql-Abfrage
        mysqlpp::StoreQueryResult r = query.store();

        //Überprüfung ob sql-Abfrage ergebnis zurück gibt sollte dies nicht der Fall sein wird geprüft, ob Ausschaltzeit vorhanden
        if( r.num_rows() == 0 )
        {
                //SQL Abfrage
                query   << " SELECT * "
                        << " FROM schaltzeiten "
                        << " WHERE mm_aus =" << mysqlpp::quote << Minute
                        << " AND hh_aus = " << mysqlpp::quote << Stunde
                        << " AND Di = '1'"
                        << " AND verbraucher = " << mysqlpp::quote << zuordnungdb;

                 //store-Funktion zur sql-Abfrage
                mysqlpp::StoreQueryResult r = query.store();

                    //Überprüfung ob Ausschaltzeit vorhanden
                    if( r.num_rows() == 0 )
                    {
                        //cout << "Keinerlei Übereinstimmung" << endl;
                        return;
                    }
                    else
                    {
                        //Setzung der Variablen
                        id = (int(r[0]["id"]));
                        verbZuordnung = (int(r[0]["verbraucher"]));
                        schaltStd = (int(r[0]["hh_aus"]));
                        schaltMin = (int(r[0]["mm_aus"]));
                        zustandVerb = 0;
                        schaltWoTag = 2;
                        break;
                    }
            }

        else
        {
            id= (int(r[0]["id"]));
            verbZuordnung = (int(r[0]["verbraucher"]));
            schaltStd = (int(r[0]["hh_an"]));
            schaltMin = (int(r[0]["mm_an"]));
            zustandVerb = 1;
            schaltWoTag = 2;
            break;
        }
    }

    case 3: //Aktueller Wochentag ist ein Mittwoch =3
    {
        mysqlpp::Query query = conn.query();

        //SQL Abfrage
        query   << " SELECT * "
                << " FROM schaltzeiten "
                << " WHERE mm_an =" << mysqlpp::quote << Minute
                << " AND hh_an = " << mysqlpp::quote << Stunde
                << " AND Mi = '1' "
                << " AND verbraucher = " << mysqlpp::quote << zuordnungdb;

        //store-Funktion zur sql-Abfrage
        mysqlpp::StoreQueryResult r = query.store();

        //Überprüfung ob sql-Abfrage ergebnis zurück gibt sollte dies nicht der Fall sein wird geprüft, ob Ausschaltzeit vorhanden
        if( r.num_rows() == 0 )
        {
            //Query präparieren
            mysqlpp::Query query = conn.query();

            //SQL Abfrage
            query   << " SELECT * "
                    << " FROM schaltzeiten "
                    << " WHERE mm_aus =" << mysqlpp::quote << Minute
                    << " AND hh_aus = " << mysqlpp::quote << Stunde
                    << " AND Mi = '1'"
                    << " AND verbraucher = " << mysqlpp::quote << zuordnungdb;

            //store-Funktion zur sql-Abfrage
            mysqlpp::StoreQueryResult r = query.store();

            //Überprüfung ob Ausschaltzeit vorhanden
            if( r.num_rows() == 0 )
            {
                //cout << "Keinerlei Übereinstimmung" << endl;
                return;
            }
            else
            {
                //Setzung der Variablen
                id = (int(r[0]["id"]));
                verbZuordnung = (int(r[0]["verbraucher"]));
                schaltStd = (int(r[0]["hh_aus"]));
                schaltMin = (int(r[0]["mm_aus"]));
                zustandVerb = 0;
                schaltWoTag = 3;
                break;
            }
        }
        else
        {
            id = (int(r[0]["id"]));
            verbZuordnung = (int(r[0]["verbraucher"]));
            schaltStd = (int(r[0]["hh_an"]));
            schaltMin = (int(r[0]["mm_an"]));
            zustandVerb = 1;
            schaltWoTag = 3;
            break;
        }
    }

    case 4: //Aktueller Wochentag ist ein Donnerstag
    {
        mysqlpp::Query query = conn.query();

        //SQL Abfrage
        query   << " SELECT * "
                << " FROM schaltzeiten "
                << " WHERE mm_an =" << mysqlpp::quote << Minute
                << " AND hh_an = " << mysqlpp::quote << Stunde
                << " AND Do = '1' "
                << " AND verbraucher = " << mysqlpp::quote << zuordnungdb;

        //store-Funktion zur sql-Abfrage
        mysqlpp::StoreQueryResult r = query.store();

        //Überprüfung ob sql-Abfrage ergebnis zurück gibt sollte dies nicht der Fall sein wird geprüft, ob Ausschaltzeit vorhanden
        if( r.num_rows() == 0 )
        {
            //Query präparieren
            mysqlpp::Query query = conn.query();

            //SQL Abfrage
            query   << " SELECT * "
                    << " FROM schaltzeiten "
                    << " WHERE mm_aus =" << mysqlpp::quote << Minute
                    << " AND hh_aus = " << mysqlpp::quote << Stunde
                    << " AND Do = '1'"
                    << " AND verbraucher = " << mysqlpp::quote << zuordnungdb;

            //store-Funktion zur sql-Abfrage
            mysqlpp::StoreQueryResult r = query.store();

                    if( r.num_rows() == 0 )
                    {
                        //cout << "Keinerlei Übereinstimmung" << endl;
                        return;
                    }
                    else
                    {
                        //Setzung der Variablen
                        id = (int(r[0]["id"]));
                        verbZuordnung = (int(r[0]["verbraucher"]));
                        schaltStd = (int(r[0]["hh_aus"]));
                        schaltMin = (int(r[0]["mm_aus"]));
                        zustandVerb = 0;
                        schaltWoTag = 4;
                        break;
                    }
        }
        else
        {
            id = (int(r[0]["id"]));
            verbZuordnung = (int(r[0]["verbraucher"]));
            schaltStd = (int(r[0]["hh_an"]));
            schaltMin = (int(r[0]["mm_an"]));
            zustandVerb = 1;
            schaltWoTag = 4;
            break;
        }
    }

    case 5: //Der aktueller Wochentag ist Freitag =5
    {
        mysqlpp::Query query = conn.query();

        //SQL Abfrage
        query   << " SELECT * "
                << " FROM schaltzeiten "
                << " WHERE mm_an =" << mysqlpp::quote << Minute
                << " AND hh_an = " << mysqlpp::quote << Stunde
                << " AND Fr = '1' "
                << " AND verbraucher = " << mysqlpp::quote << zuordnungdb;

        //store-Funktion zur sql-Abfrage
        mysqlpp::StoreQueryResult r = query.store();

        //Überprüfung ob sql-Abfrage ergebnis zurück gibt sollte dies nicht der Fall sein wird geprüft, ob Ausschaltzeit vorhanden
        if( r.num_rows() == 0 )
        {
            //Query präparieren
            mysqlpp::Query query = conn.query();

            //SQL Abfrage
            query   << " SELECT * "
                    << " FROM schaltzeiten "
                    << " WHERE mm_aus =" << mysqlpp::quote << Minute
                    << " AND hh_aus = " << mysqlpp::quote << Stunde
                    << " AND Fr = '1'"
                    << " AND verbraucher = " << mysqlpp::quote << zuordnungdb;

            //store-Funktion zur sql-Abfrage
            mysqlpp::StoreQueryResult r = query.store();


            //Überprüfung ob Ausschaltzeit vorhanden
            if( r.num_rows() == 0 )
            {
                //cout << "Keinerlei Übereinstimmung" << endl;
                return;
            }
            else
            {
                //Setzung der Variablen
                id = (int(r[0]["id"]));
                verbZuordnung = (int(r[0]["verbraucher"]));
                schaltStd = (int(r[0]["hh_aus"]));
                schaltMin = (int(r[0]["mm_aus"]));
                zustandVerb = 0;
                schaltWoTag = 5;
                break;
            }
        }

        else
        {
            id = (int(r[0]["id"]));
            verbZuordnung = (int(r[0]["verbraucher"]));
            schaltStd = (int(r[0]["hh_an"]));
            schaltMin = (int(r[0]["mm_an"]));
            zustandVerb = 1;
            schaltWoTag = 5;
            break;
        }
    }

    case 6: //Aktueller Wochentag ist ein Samstag = 6
    {
        mysqlpp::Query query = conn.query();

        //SQL Abfrage
        query   << " SELECT * "
                << " FROM schaltzeiten "
                << " WHERE mm_an =" << mysqlpp::quote << Minute
                << " AND hh_an = " << mysqlpp::quote << Stunde
                << " AND Sa = '1'"
                << " AND verbraucher = " << mysqlpp::quote << zuordnungdb;

        //store-Funktion zur sql-Abfrage
        mysqlpp::StoreQueryResult r = query.store();

        //Überprüfung ob sql-Abfrage ergebnis zurück gibt sollte dies nicht der Fall sein wird geprüft, ob Ausschaltzeit vorhanden
        if( r.num_rows() == 0 )
        {
            //Query präparieren
            mysqlpp::Query query = conn.query();

            //SQL Abfrage
            query   << " SELECT * "
                    << " FROM schaltzeiten "
                    << " WHERE mm_aus =" << mysqlpp::quote << Minute
                    << " AND hh_aus = " << mysqlpp::quote << Stunde
                    << " AND Sa = '1'"
                    << " AND verbraucher = " << mysqlpp::quote << zuordnungdb;

            //store-Funktion zur sql-Abfrage
            mysqlpp::StoreQueryResult r = query.store();

            //Überprüfung ob Ausschaltzeit vorhanden
            if( r.num_rows() == 0 )
            {
                //cout << "Keinerlei Übereinstimmung" << endl;
                return;
            }

            else
            {
               //Setzung der Variablen
                id = (int(r[0]["id"]));
                verbZuordnung = (int(r[0]["verbraucher"]));
                schaltStd = (int(r[0]["hh_aus"]));
                schaltMin = (int(r[0]["mm_aus"]));
                zustandVerb = 0;
                schaltWoTag = 6;
                break;
            }

        }
        else
        {
            id = (int(r[0]["id"]));
            verbZuordnung = (int(r[0]["verbraucher"]));
            schaltStd = (int(r[0]["hh_an"]));
            schaltMin = (int(r[0]["mm_an"]));
            zustandVerb = 1;
            schaltWoTag = 6;
            break;
        }
    }
    }
}


/**
 * Gibt die Stunde aus in der laut Schaltplan geschalten werden soll
 * @return int
 */
int DBKommunikation::Schaltstunde()
{
    return schaltStd;
}


/**
 * Gibt die Minute aus nach der laut Schaltplan geschalten werden soll
 * @return int
 */
int DBKommunikation::Schaltminute()
{
    return schaltMin;
}


/**
 * Gibt den geplanten Verbraucherzustand aus
 * @return int
 */
int DBKommunikation::Verbraucherzustand()
{
    return zustandVerb;
}


/**
 * Gibt an um welchen Verbraucher es sich handelt
 * @return int
 */
int DBKommunikation::Verbraucherzuordnung()
{
    return verbZuordnung;
}


/**
 * Gibt aus ob der Urlaubsmodus gesetzt ist 1= aktiv 0=aus
 * @return int
 */
int DBKommunikation::Urlaubsmoduszustand()
{
    //Query präparieren
    mysqlpp::Query query = conn.query();

    //SQL Abfrage
    query   << " SELECT * "
            << " FROM urlaubsmodus;";


    //store-Funktion zur sql-Abfrage
    mysqlpp::StoreQueryResult r = query.store();
    if( r.num_rows() == 0 )
            {
                //cout << "Keinerlei Übereinstimmung" << endl;
                return urlaubMod=0;
            }
    //cout << "Urlaubsmodus" << urlaubMod <<endl;
    urlaubMod = (int(r[0]["status"]));
    return urlaubMod;
}


/**
 * Gibt den geplanten Schaltwochentag aus
 * @return int
 */
int DBKommunikation::SchaltWoTag()
{
    return schaltWoTag;
}


/**
 * Gibt die ID des Datenbankeintrags aus
 * @return int
 */
int DBKommunikation::Id()
{
    return id;
}


/**
 * Fügt den Ausschaltzeitpunkt des Timers in die Tabelle Schaltzeiten hinzu
 * @param  Id gibt die geplante Id ein
 * @param  VerbraucherZuordnung Gibt den Verbraucher an, der sich im Timermodus
 * befindet
 * @param  SchaltStunde Stunde, in der Timer ausgehen soll
 * @param  SchaltMinute Minute, in der der Timer und somit der Verbraucher ausgehen
 * soll
 * @param  SchaltWoTag Wochentag an dem Timer und Verbraucher ausgehen sollen
 */
void DBKommunikation::Modus4Hinzu(int Id, int VerbraucherZuordnung, int SchaltStunde, int SchaltMinute, int SchaltWoTag)
{
    // Zustand des Verbrauchers auf 0 setzen und ID auf 101 für Verbraucher 1 oder 102 für Verbraucher 2 für die Wiedererkennung
    switch (SchaltWoTag)
    {
    case 0: //Aktueller Wochentag Sonntag
    {
        mysqlpp::Query query = conn.query();
        query << "insert into schaltzeiten (id, verbraucher, hh_an, mm_an, hh_aus, mm_aus, Mo, Di, Mi, Do, Fr, Sa, So) values"
              << "('"
              << mysqlpp::quote << Id
              << "','"
              << mysqlpp::quote << VerbraucherZuordnung
              << "',"
              << "0, 0, '"
              << mysqlpp::quote << SchaltStunde
              << "','"
              << mysqlpp::quote << SchaltMinute
              << " ',0,0,0,0,0,0,1);";

        query.execute();
        break;
    }

    case 1: //Aktueller Wochentag Montag
    {
        mysqlpp::Query query = conn.query();
        query << "insert into schaltzeiten (id, verbraucher, hh_an, mm_an, hh_aus, mm_aus, Mo, Di, Mi, Do, Fr, Sa, So) values"
              << "('"
              << mysqlpp::quote << Id
              << "','"
              << mysqlpp::quote << VerbraucherZuordnung
              << "',"
              << "0, 0, '"
              << mysqlpp::quote << SchaltStunde
              << "','"
              << mysqlpp::quote << SchaltMinute
              << " ',1,0,0,0,0,0,0);";

        query.execute();
        break;
    }

    case 2: //Aktueller Wochentag Dienstag
    {
        mysqlpp::Query query = conn.query();
        query << "insert into schaltzeiten (id, verbraucher, hh_an, mm_an, hh_aus, mm_aus, Mo, Di, Mi, Do, Fr, Sa, So) values"
              << "('"
              << mysqlpp::quote << Id
              << "','"
              << mysqlpp::quote << VerbraucherZuordnung
              << "',"
              << "0, 0, '"
              << mysqlpp::quote << SchaltStunde
              << "','"
              << mysqlpp::quote << SchaltMinute
              << " ',0,1,0,0,0,0,0);";

        query.execute();
        break;
    }

    case 3: //Aktueller Wochentag Mittwoch
    {
        mysqlpp::Query query = conn.query();
        query << "insert into schaltzeiten (id, verbraucher, hh_an, mm_an, hh_aus, mm_aus, Mo, Di, Mi, Do, Fr, Sa, So) values"
              << "('"
              << mysqlpp::quote << Id
              << "','"
              << mysqlpp::quote << VerbraucherZuordnung
              << "',"
              << "0, 0, '"
              << mysqlpp::quote << SchaltStunde
              << "','"
              << mysqlpp::quote << SchaltMinute
              << " ',0,0,1,0,0,0,0);";

        query.execute();
        break;
    }

    case 4: //Aktueller Wochentag Donnerstag
    {
        mysqlpp::Query query = conn.query();
        query << "insert into schaltzeiten (id, verbraucher, hh_an, mm_an, hh_aus, mm_aus, Mo, Di, Mi, Do, Fr, Sa, So) values"
              << "('"
              << mysqlpp::quote << Id
              << "','"
              << mysqlpp::quote << VerbraucherZuordnung
              << "',"
              << "0, 0, '"
              << mysqlpp::quote << SchaltStunde
              << "','"
              << mysqlpp::quote << SchaltMinute
              << " ',0,0,0,1,0,0,0);";

        query.execute();
        break;
    }

    case 5: //AKtueller Wochentag Freitag
    {
        mysqlpp::Query query = conn.query();
        query << "insert into schaltzeiten (id, verbraucher, hh_an, mm_an, hh_aus, mm_aus, Mo, Di, Mi, Do, Fr, Sa, So) values"
              << "('"
              << mysqlpp::quote << Id
              << "','"
              << mysqlpp::quote << VerbraucherZuordnung
              << "',"
              << "0, 0, '"
              << mysqlpp::quote << SchaltStunde
              << "','"
              << mysqlpp::quote << SchaltMinute
              << " ',0,0,0,0,1,0,0);";

        query.execute();
        break;
    }

    case 6: //Aktueller Wochentag Samstag
    {
        mysqlpp::Query query = conn.query();
        query << "insert into schaltzeiten (id, verbraucher, hh_an, mm_an, hh_aus, mm_aus, Mo, Di, Mi, Do, Fr, Sa, So) values"
              << "('"
              << mysqlpp::quote << Id
              << "','"
              << mysqlpp::quote << VerbraucherZuordnung
              << "',"
              << "0, 0, '"
              << mysqlpp::quote << SchaltStunde
              << "','"
              << mysqlpp::quote << SchaltMinute
              << " ',0,0,0,0,0,1,0);";

        query.execute();
        break;
    }
    }
}


/**
 * Nach Erreichen des Timerendes wird der Eintrag im Schaltplan gelöscht
 */
void DBKommunikation::Modus4Loeschen()
{

    mysqlpp::Query query = conn.query();
    if(zuordnungdb == 1)
    {
        query << "delete from schaltzeiten where id = '101';";
    }
    if(zuordnungdb == 2)
    {
        query << "delete from schaltzeiten where id = '102';";
    }
    query.execute();

}


/**
 * Gibt true zurück, wenn Speicher bereit ist
 * Quelle: Vertiefungslabor Herr Trageser
 * @return bool
 */
bool DBKommunikation::IsReady() const
{
    return ready;
}


/**
 * Holt den Tastermodus aus Datenbank
 * @return int
 */
int DBKommunikation::HoleTastMod ()
{
    mysqlpp::Query query = conn.query();
    query   << " SELECT * "
            << " FROM tastermodus "
            << " WHERE verbraucher = '" << mysqlpp::quote << zuordnungdb << "';";

    //store-Funktion zur sql-Abfrage
    mysqlpp::StoreQueryResult r = query.store();

    //Pruefen, ob Zeile vorhanden
    if( r.num_rows() == 0 )
    {
        //cout << "Keinerlei Übereinstimmung" << endl;
        return tasterMod = 0;
    }
    else
    {
        //cout<<"Gefunden!"<<endl;
        tasterMod = (int(r[0]["modus"]));
        return tasterMod;
    }

}

/**
 * Übergibt an Datenbank den neuen Zustand
 * @param neuer Zustand übergibt den neuen Zustand
 */
void DBKommunikation::Verbraucherzustaende( int NeuerZustand )
{
    //cout << "Verbruacherzustaende funktion" <<endl;
    mysqlpp::Query query = conn.query();

    query << "update verbraucherzustaende set zustand ='"
          << mysqlpp::quote << NeuerZustand
          << "'"
          << "where verbraucher ='"
          << mysqlpp::quote << zuordnungdb
          << "';";

    query.execute();
    return;

}


/**
* Löscht die Einträge in der Schalthistorie vor 14 Tagen
*/
void DBKommunikation::LoescheEintraege()
{


    mysqlpp::Query query = conn.query();

    query << "delete from schalthistorie where DATEDIFF(NOW(), zeitpunkt) >= '14';";

    query.execute();

}
