/** \brief Dateiinformationen Urlaubsmodus.h

Projekt:	Zeitschaltuhr
Hersteller:	Timecode 22 GmbH

Erstellt von:	@authr Anja Wenk
Datum:		    2015.12.08
Revision:	    Raphael Kuttruf
Datum:		    2016.03.02

Headerdatei der Klasse Urlaubsmodus
*/

#ifndef URLAUBSMODUS_H
#define URLAUBSMODUS_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

using namespace std;

class Urlaubsmodus
{


public:


    /**
     * Konstruktor des Urlaubsmoduses
     */
    Urlaubsmodus ();


    /**
     * Berechnet zufällig den Wert um den die Schaltzeit verändert werden soll
     */
    void BerechAenderung ();


    /**
     * Gibt die zufällige Verzögerung der Schaltzeit zwischen einer und zehn Minuten aus
     * @return int
     */
    int Aenderung ();


    /**
     * Berechnet zufällig, ob die Änderung subtrahiert oder addiert wird zur Schaltzeit
     */
    void Zufallssumme ();


    /**
     * Gibt das Ergebnis aus der Zufallssumme zurück
     * @return int
     */
    int SuboderAdd ();

protected:

public:

protected:

public:

protected:


private:

    // Private attributes
    //

    // die berechnete Verscheibung des Schaltzeitpunktes im Urlaubsmodus
    int aendZeit;

    // gibt an ob subtrahiert (2) oder addiert wird (1)
    int subadd;

public:

private:

public:

private:



};

#endif // URLAUBSMODUS_H
