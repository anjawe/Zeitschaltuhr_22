/** \brief Dateiinformationen DBKommunikation.h

Projekt:	Zeitschaltuhr
Hersteller:	Timecode 22 GmbH

Erstellt von:	@authr Anja Wenk
Datum:		    2015.12.08
Revision:	    Raphael Kuttruf
Datum:		    2016.03.02

Headerdatei der Klasse DBKommunikation
*/

#ifndef DBKOMMUNIKATION_H
#define DBKOMMUNIKATION_H

#include "mysql++.h"
#include <iostream>
#include "stdlib.h"
#include "stdio.h"


using namespace std;

//Quelle: Vertiefungslabor Trageser ET14A

class DBKommunikation //Für das Auslesen des Schaltplans zuständig
{
public:

    /**
     * Konstruktor der Klasse DB-Kommunikation
     */
    DBKommunikation();


    /**
     * Spezieller Konstruktor
     * @param  Zuordnung Zuordnung zu welchem Objekt gehörig
     * @param  User Speichert den User der Datenbank ab
     * @param  Passwd Speichert das Passwort des Users
     */
    DBKommunikation (int Zuordnung, const string& User, const string& Passwd) ;


    /**
    * Schaltet den Verbraucher im Falle eines Stromausfalls etc auf den letzten
    * gespeicherten Zustand, der in der Datenbank gespeichert ist
    */
    void StartZustandVerb();

    /**
    * Trägt die Verbraucherzustandsänderung in die Datenbank ein
    * @param  VerbraucherZuordnung Gibt an um welchen Verbraucher es sich handelt
    * @param  VerbraucherZust Gibt den Zustand an, wie sich der Verbraucher geändert
    * hat; 1= Verbraucher wurde angeschalten
    */
    void SchalthistorienEintrag (int VerbraucherZuordnung, int VerbraucherZust);


    /**
     * Aktualisiert die Variablen der Klasse DBKommunikation für den nächsten
     * Schaltzeitpunkt in 5 Minuten
     * @param  Stunde Suche nach Stunde, in der Verbraucher schalten soll nach
     * Schaltplan
     * @param  Minute Suche nach Minute in der sich der Verbraucher ändern soll laut
     * Schaltplan
     * @param  WoTag Wochentag nachdem in Datenbank gesucht werden soll
     */
    void AktualisiereDB(int Stunde, int Minute, int Wotag);


    /**
     * Gibt die Stunde aus in der laut Schaltplan geschalten werden soll
     * @return int
     */
    int Schaltstunde ();


    /**
     * Gibt die Minute aus nach der laut Schaltplan geschalten werden soll
     * @return int
     */
    int Schaltminute ();


    /**
     * Gibt den geplanten Verbraucherzustand aus
     * @return int
     */
    int Verbraucherzustand ();


    /**
    * Gibt an um welchen Verbraucher es sich handelt
    * @return int
    */
    int Verbraucherzuordnung ();


    /**
     * Gibt den Urlaubsmoduszustand aus; 1=aktiv
     * @return int
     */
    int Urlaubsmoduszustand ();


    /**
     * Gibt den geplanten Schaltwochentag aus
     * @return int
     */
    int SchaltWoTag ();

    /**
     * Gibt die Id des Datenbankeintrags aus
     * @return int
     */
    int Id ();


    /**
     * Fügt den Ausschaltzeitpunkt des Timers in die Tabelle Schaltzeiten hinzu
     * @param  Id fügt Id ein für Wiedererkennung
     * @param  VerbraucherZuordnung Gibt den Verbraucher an, der sich im Timermodus
     * befindet
     * @param  SchaltStunde Stunde, in der Timer ausgehen soll
     * @param  SchaltMinute Minute, in der der Timer und somit der Verbraucher ausgehen
     * soll
     * @param  SchaltWoTag Wochentag an dem Timer und Verbraucher ausgehen sollen
     */
    void Modus4Hinzu(int Id, int VerbraucherZuordnung, int SchaltStunde, int SchaltMinute, int SchaltWoTag);

    /**
     * Nach Erreichen des Timerendes wird der Eintrag im Schaltplan gelöscht
     */
    void Modus4Loeschen ();

    /**
      * Gibt true zurück, wenn Speicher bereit ist
      * Quelle: Vertiefungslabor Herr Trageser
      * @return bool
      */
    bool IsReady () const;


    /**
    * Holt den Tastermodus aus Datenbank
    * @return int
    */
    int HoleTastMod ();


    /**
    * Übergibt an Datenbank den neuen Zustand
    * @param neuer Zustand Gibt den neuen Zustand des Verbrauchers an
    */
    void Verbraucherzustaende(int NeuerZustand);


    /**
    * Löscht die Einträge in der Schalthistorie vor 14 Tagen
    */

    void LoescheEintraege ();


protected:
    // Speicherobjekt
    // Quelle: Vertiefungslabor Herr Trageser
    mysqlpp::Connection conn;

    // Speicher bereit
    // Quelle: Vertiefungslabor Herr Trageser
    bool ready;

public:

protected:

public:

protected:


private:

    // Ordnet dem Objekt eine Zugehörigkeit zu; 1= Verbraucher 1
    int zuordnungdb;

    // Stunde in der geschalten werden soll laut Schaltplan
    int schaltStd;

    // Minute in der geschalten werden soll laut Schaltplan
    int schaltMin;

    // Wochentag an dem geschalten werden soll laut Schaltplan
    int schaltWoTag;

    // Zuordnung welcher Verbraucher geschalten werden soll
    int verbZuordnung;

    // Zustand in den Verbraucher geschalten werden soll
    int zustandVerb;

    // Aktueller Urlaubsmodus aus Datenbank
    int urlaubMod;

    // aktueller Tastermodus aus Datenbank
    int tasterMod;

    // aktueller Tasterzuszand aus Datenbank ( Simulation)
    int tasterZustand;

    // Id des Schaltplaneintrags
    int id;


public:

private:

public:

private:



};

#endif // DB_KOMMUNIKATION_H
