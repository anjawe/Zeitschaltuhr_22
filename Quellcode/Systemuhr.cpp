/** \brief Dateiinformationen Systemuhr.cpp

Projekt:	Zeitschaltuhr
Hersteller:	Timecode 22 GmbH

Erstellt von:	@authr Anja Wenk
Datum:		    2015.12.08
Revision:	    Raphael Kuttruf
Datum:		    2016.03.02

cpp-Datei der Klasse Systemuhr
*/

#include "Systemuhr.h"



using namespace std;
//
// Methods
//


// Accessor methods
//


// Other methods
//


/**
 * Konstruktor der Klasse Systemuhr
 */
Systemuhr::Systemuhr ()
{
}


/**
 * Gibt die aktuelle Uhrzeit, Datumstag und den Wochentag aus.
 */
void Systemuhr::AktZeit ()
{
    time_t timer = time(0);
    struct tm * tblock = localtime(&timer);
    aktStunden = tblock->tm_hour; //Stunden aktuell
    aktMinuten = tblock->tm_min; //Minuten aktuell
    aktWoTag = tblock->tm_wday; //Wochentag 1=Montag 0=Sonntag
    aktTag = tblock->tm_mday; //Datum Tag (1-31)
    aktSekunden = tblock->tm_sec; //Sekunden aktuell
    aktMon = tblock->tm_mon + 1; //Aktueller Monat (1-12)

}


/**
 * Gibt aktuelle Minuten zurück
 * @return int
 */
int Systemuhr::AktMin ()
{
    return aktMinuten;
}


/**
 * Gibt aktuelle Stunde zurück
 * @return int
 */
int Systemuhr::AktStd ()
{
    return aktStunden;
}


/**
 * Gibt aktuellen Tag im Monat zurück (Datumstag)
 * @return int
 */
int Systemuhr::AktTagMon ()
{
    return aktTag;
}


/**
 * Gibt aktuellen Wochentag zurück
 * @return int
 */
int Systemuhr::AktTagWoche ()
{
    return aktWoTag;
}


/**
 * Gibt aktuelle Sekunden zurück
 * @return int
 */
int Systemuhr::AktSek ()
{
    return aktSekunden;
}


/**
 * Gibt aktuellen Monat zurück
 * @return int
 */
int Systemuhr::AktMonat()
{
    return aktMon;
}
