/** \brief Dateiinformationen

Projekt:	Zeitschaltuhr
Hersteller:	Timecode 22 GmbH

Erstellt von:	@authr Anja Wenk
Datum:		    2015.12.08
Revision:	    Raphael Kuttruf
Datum:		    2016.03.02

Main-Datei
*/

#include <iostream>
#include "DBKommunikation.h"
#include "Verbraucher.h"
#include "Systemuhr.h"
#include "Urlaubsmodus.h"
#include <wiringPi.h>

using namespace std;

int main()
{

    //Wiring Pi Setup prüfen

    if( wiringPiSetup() == -1)
    {
       cout << "Fehler Pi!" << endl;
       return 1;
    }

    //Pinfestlegung
    const int ledV1 = 3;    //Real: Pin = 15 GPIO 22 ; Verbraucher 1
    const int ledV2 = 4;    //Real: Pin = 16 GPIO 23 ; Verbraucher 2
    const int ledT1 = 28;   //Real: Pin = 38 GPIO 20 ; Taster 1
    const int ledT2 = 29;   //Real: Pin = 40 GPIO 21 ; Taster 2
    const int ledExt1 = 0;  //Real: Pin = 11 GPIO  17; App Zugriff für V1
    const int ledExt2 = 1;  //Real: Pin = 12 GPIO  18; App Zugriff für V2



    // Pins als Ausgang oder Eingang deklarieren
    //Verbraucher 1 = Ausgang
    pinMode(ledV1, OUTPUT);

    //Verbraucher 2 = Ausgang
    pinMode(ledV2, OUTPUT);

    //Taster 1 als Eingang
    pinMode(ledT1, INPUT);

    //Taster 2 als Eingang
    pinMode(ledT2, INPUT);

    //App Zugriff für V1 als Eingang
    pinMode(ledExt1, INPUT);

    //App Zugriff für V2 als Eingang
    pinMode(ledExt2, INPUT);


    //Initialisierung der Objekte und setzen der Komposition
    //Verbaucherinitalisierung und statische Variableninitialiserung
    Verbraucher v1(1, ledV1, ledT1);
    Verbraucher v2(2, ledV2, ledT2);

    DBKommunikation dbkom(1, "root", "123456");
    v1.SetzeDB(dbkom);

    DBKommunikation dbkom2(2,"root", "123456");
    v2.SetzeDB(dbkom2);


    //Start Änderung berechnen für den Urlaubsmodus
    v1.AenderungBerechnen();
    v2.AenderungBerechnen();


    // Pruefen, ob DB-Verbindung bereit ist
    if( !dbkom.IsReady() )
    {
        cerr << "Datenbank ist nicht bereit." << endl;
        return 0;
    }
    if( !dbkom2.IsReady() )
    {
        cerr << "Datenbank ist nicht bereit." << endl;
        return 0;
    }

    //Startzustand Verbraucher
    dbkom.StartZustandVerb();
    dbkom2.StartZustandVerb();
    int verb1 = dbkom.Verbraucherzustand();
    int verb2 = dbkom2.Verbraucherzustand();
    v1.SetzteVerbraucher(verb1);
    v2.SetzteVerbraucher(verb2);

    //Setzte Systemzustand des Verbruachers auf echten Zustand der LED
    int led1 = 0;
    int led2 = 0;

    //Eingang der Taster
    int eingang1 = 0;
    int eingang2 = 0;



    while(1) // Zyklischer Programmablauf
    {
        v1.AktZeit();
        v2.AktZeit();

        eingang1 = digitalRead(ledT1);

        if(eingang1 == 1)
        {
            v1.Tastermodus();
        }

        eingang2 = digitalRead(ledT2);

        if(eingang2 == 1)
        {
            v2.Tastermodus();
        }


        v1.HoleSchaltzeit();

        v2.HoleSchaltzeit();


        //Fernzugriff V1
        if (digitalRead(ledExt1) == 1)
        {
            v1.Fernzugriff();
            digitalWrite(ledExt1,0);
        }
        //Fernzugriff V2
        if (digitalRead(ledExt2) == 1)
        {
            v2.Fernzugriff();
            digitalWrite(ledExt2,0);
        }

        // Bewusste verlangsamung des Programmdurchlaufs
        delay(500);

    }
    return 0;
}
