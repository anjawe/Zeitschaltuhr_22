var class_d_b_kommunikation =
[
    [ "DBKommunikation", "class_d_b_kommunikation.html#a7a3da91633382a77d47351ea25dea7a3", null ],
    [ "DBKommunikation", "class_d_b_kommunikation.html#a3aeec549ddcbc44498cf4857c9360f9a", null ],
    [ "AktualisiereDB", "class_d_b_kommunikation.html#a8fee8859684e256bc87bdb243b0fce90", null ],
    [ "HoleTastMod", "class_d_b_kommunikation.html#a2e38d5ca277566785e28b7127d9cba57", null ],
    [ "Id", "class_d_b_kommunikation.html#a5ddc599a5313cfec8d8e817f9d2d6cda", null ],
    [ "IsReady", "class_d_b_kommunikation.html#ac48595bc28f4caf4d5adfd1bc8fc07cd", null ],
    [ "LoescheEintraege", "class_d_b_kommunikation.html#a3fda6d5619276e7a9594ec7834a4a173", null ],
    [ "Modus4Hinzu", "class_d_b_kommunikation.html#a7f97a55b53e4404d4065f2622042a415", null ],
    [ "Modus4Loeschen", "class_d_b_kommunikation.html#ad4c06d92a9b9af82a46f0aad703a3a32", null ],
    [ "PruefeTasteingang", "class_d_b_kommunikation.html#a11471cb3126ea9e9beb74b88050823c0", null ],
    [ "SchalthistorienEintrag", "class_d_b_kommunikation.html#aded313b424cc2f88b9255a59ed1e34bd", null ],
    [ "Schaltminute", "class_d_b_kommunikation.html#a823816ccdaa5229db9aa4bd6f9fb93e6", null ],
    [ "Schaltstunde", "class_d_b_kommunikation.html#aea8a0759d57c1496841997652dfc9536", null ],
    [ "SchaltWoTag", "class_d_b_kommunikation.html#abf3c165cfdcbbd0f86c216f4d034e42d", null ],
    [ "StartZustandVerb", "class_d_b_kommunikation.html#a795cf14b14d9b2dc6e8264ef5c910cf4", null ],
    [ "Urlaubsmoduszustand", "class_d_b_kommunikation.html#a2d4ba6bca61ef03811b597e0e10a9c4e", null ],
    [ "Verbraucherzuordnung", "class_d_b_kommunikation.html#a3892071a55d9f97014f3a0f3846d70c4", null ],
    [ "Verbraucherzustaende", "class_d_b_kommunikation.html#a1a111d33c91ec50dab3f62e80c78bc80", null ],
    [ "Verbraucherzustand", "class_d_b_kommunikation.html#ac4ac712b2b2ba09e1e32e9ac8266b2b8", null ],
    [ "conn", "class_d_b_kommunikation.html#ad3d2d04d81b4780f5aeea3498cb54ef0", null ],
    [ "ready", "class_d_b_kommunikation.html#a39805a3fc96087481d960288eac30855", null ]
];