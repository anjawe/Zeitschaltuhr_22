var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Startseite'});
});

router.get('/index', function(req, res, next) {
  res.render('index', { title: 'Startseite'});
});

router.get('/zeitplan', function(req, res, next) {
  res.render('zeitplan', { title: 'Zeitplan'});
  
});

router.get('/tastermodus', function(req, res, next) {
  res.render('tastermodus', { title: 'Tastermodus'});
});

router.get('/schalthistorie', function(req, res, next) {
 res.render('schalthistorie', { title: 'Schalthistorie' });
});

router.get('/impressum', function(req, res, next) {
  res.render('impressum', { title: 'Impressum' });
});

module.exports = router;
