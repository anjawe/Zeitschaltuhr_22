/// socket-Events für den Tastermodus

/// Projekt:	Zeitschaltuhr
/// Hersteller:	Timecode 22 GmbH

/// Erstellt von:	Lena Kopp
/// Datum:		    2015.12.19
/// Revision:			~Name
/// Datum:				JJJJ.MM.DD
/// Qualitätscheck:	~Name
/// Datum:		    JJJJ.MM.DD



$(document).ready(function()
{
    var modi;
    var verb1Mod = new Array ("#deakt1","#tast1","#schalt1","#time1");
    var verb2Mod = new Array ("#deakt2","#tast2","#schalt2","#time2");

    //gibt den Tastermodus für Verbraucher 1 aus
    socket.emit('leseTastermodus','true');
    socket.on('taster1', function(modus1)
    {
        switch (modus1)
        {
        case 1:
            $('#deakt1').attr("checked","checked");
            break;
        case 2:
            $('#tast1').attr("checked","checked");
            break;
        case 3:
            $('#schalt1').attr("checked","checked");
            break;
        case 4:
            $('#time1').attr("checked","checked");
            break;
        }

    });
    socket.on('taster2', function(modus1)
    {
        switch (modus1)
        {
        case 1:
            $('#deakt2').attr("checked","checked");
            break;
        case 2:
            $('#tast2').attr("checked","checked");
            break;
        case 3:
            $('#schalt2').attr("checked","checked");
            break;
        case 4:
            $('#time2').attr("checked","checked");
            break;
        }

    });

    //Tastermodus in Datenbank schreiben
    $('#leseTaster').click(function()
    {

        //löscht die Einträge in der Datenbank
        socket.emit('löscheTaster','true');

        for(modi = 0; modi < verb1Mod.length; modi++)
        {

            //liest den Tastermodus für Verbraucher 1 ein
            if($(verb1Mod[modi]).is(':checked'))
            {
                var modus1 = $(verb1Mod[modi]).val();

                var taster1 =
                {
                    verb    :   1,
                    modus   :   modus1
                }

                socket.emit('tastermodus',taster1);
            }

            //liest den Tastermodus für Verbraucher 2 ein
            if($(verb2Mod[modi]).is(':checked'))
            {
                var modus2 = $(verb2Mod[modi]).val();

                var taster2 =
                {
                    verb    :   2,
                    modus   :   modus2
                }

                socket.emit('tastermodus',taster2);
            }

        }
    });


});
