/// aktuelles Datum und Zeit, socket.io initialisieren

/// Projekt:	Zeitschaltuhr
/// Hersteller:	Timecode 22 GmbH

/// Erstellt von:	Lena Kopp
/// Datum:		    2016.01.10
/// Revision:	 		~Name   
/// Datum:				JJJJ.MM.DD		    
/// Qualitätscheck:	~Name
/// Datum:		    JJJJ.MM.DD

var socket = io();

socket.on('datum', function(data){

	$('#datum').text(data.datum);
	$('#zeit').text(data.zeit);
	
});
