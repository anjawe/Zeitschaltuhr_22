/// \brief Funktionen und socket-Events des Zeitplanes

/// Projekt:	Zeitschaltuhr
/// Hersteller:	Timecode 22 GmbH

/// Erstellt von:	Lena Kopp
/// Datum:		    2015.12.19
/// Revision:			~Name	    
/// Datum:		    JJJJ.MM.DD
/// Qualitätscheck:	~Name
/// Datum:		    JJJJ.MM.DD


$(document).ready(function(){

 $('#timepicker1').timepicker({
 	showMeridian	: false,
 	defaultTime		: '00:00',
 	showInputs		: false,
 	minuteStep		: 1
 });

 $('#timepicker2').timepicker({
 	showMeridian	: false,
 	defaultTime		: '00:00',
 	showInputs		: false,
 	minuteStep		: 1
 });
 
 $('#timepicker3').timepicker({
 	showMeridian	: false,
 	defaultTime		: '00:00',
 	showInputs		: false,
 	minuteStep		: 1
 });
 
 $('#timepicker4').timepicker({
 	showMeridian	: false,
 	defaultTime		: '00:00',
 	showInputs		: false,
 	minuteStep		: 1
 });
 
 $('#timepicker5').timepicker({
 	showMeridian	: false,
 	defaultTime		: '00:00',
 	showInputs		: false,
 	minuteStep		: 1
 });
 
 $('#timepicker6').timepicker({
 	showMeridian	: false,
 	defaultTime		: '00:00',
 	showInputs		: false,
 	minuteStep		: 1
 });
 
 $('#timepicker7').timepicker({
 	showMeridian	: false,
 	defaultTime		: '00:00',
 	showInputs		: false,
 	minuteStep		: 1
 });
 
 $('#timepicker8').timepicker({
 	showMeridian	: false,
 	defaultTime		: '00:00',
 	showInputs		: false,
 	minuteStep		: 1
 });
 


 //Schreiben des akuellen Zeitplans
 socket.emit('leseZeit','true');
	
 socket.on('urlaubsmodus',function(urlaub){
	if(urlaub.status == 1){
	
		$('#Urlaubsmodus').attr('checked', true);
	}
	
 });
	
	socket.on('zeile', function(zeile){
	
		if(zeile.id == 1){
			$('#timepicker1').timepicker('setTime', zeile.anStd + ':' + zeile.anMin);
			$('#timepicker2').timepicker('setTime', zeile.ausStd + ':' + zeile.ausMin);
		
			if(zeile.Mo == 1){
				$('#Mo1').attr('checked', true);
			}
			if(zeile.Di == 1){
				$('#Di1').attr('checked', true);
			}
			if(zeile.Mi == 1){
				$('#Mi1').attr('checked', true);
			}
			if(zeile.Do == 1){
				$('#Do1').attr('checked', true);
			}
			if(zeile.Fr == 1){
				$('#Fr1').attr('checked', true);
			}
			if(zeile.Sa == 1){
				$('#Sa1').attr('checked', true);
			}
			if(zeile.So == 1){
				$('#So1').attr('checked', true);
			}
		}
		
		if(zeile.id == 2){
			$('#timepicker3').timepicker('setTime', zeile.anStd + ':' + zeile.anMin);
			$('#timepicker4').timepicker('setTime', zeile.ausStd + ':' + zeile.ausMin);;
		
			if(zeile.Mo == 1){
				$('#Mo2').attr('checked', true);
			}
			if(zeile.Di == 1){
				$('#Di2').attr('checked', true);
			}
			if(zeile.Mi == 1){
				$('#Mi2').attr('checked', true);
			}
			if(zeile.Do == 1){
				$('#Do2').attr('checked', true);
			}
			if(zeile.Fr == 1){
				$('#Fr2').attr('checked', true);
			}
			if(zeile.Sa == 1){
				$('#Sa2').attr('checked', true);
			}
			if(zeile.So == 1){
				$('#So2').attr('checked', true);
			}
		}
	
	if(zeile.id == 3){
		$('#timepicker5').timepicker('setTime', zeile.anStd + ':' + zeile.anMin);
		$('#timepicker6').timepicker('setTime', zeile.ausStd + ':' + zeile.ausMin);
		
		if(zeile.Mo == 1){
			$('#Mo3').attr('checked', true);
		}
		if(zeile.Di == 1){
			$('#Di3').attr('checked', true);
		}
		if(zeile.Mi == 1){
			$('#Mi3').attr('checked', true);
		}
		if(zeile.Do == 1){
			$('#Do3').attr('checked', true);
		}
		if(zeile.Fr == 1){
			$('#Fr3').attr('checked', true);
		}
		if(zeile.Sa == 1){
			$('#Sa3').attr('checked', true);
		}
		if(zeile.So == 1){
			$('#So3').attr('checked', true);
		}
	}
	
	if(zeile.id == 4){
		$('#timepicker7').timepicker('setTime', zeile.anStd + ':' + zeile.anMin);
		$('#timepicker8').timepicker('setTime', zeile.ausStd + ':' + zeile.ausMin);
		
		if(zeile.Mo == 1){
			$('#Mo4').attr('checked', true);
		}
		if(zeile.Di == 1){
			$('#Di4').attr('checked', true);
		}
		if(zeile.Mi == 1){
			$('#Mi4').attr('checked', true);
		}
		if(zeile.Do == 1){
			$('#Do4').attr('checked', true);
		}
		if(zeile.Fr == 1){
			$('#Fr4').attr('checked', true);
		}
		if(zeile.Sa == 1){
			$('#Sa4').attr('checked', true);
		}
		if(zeile.So == 1){
			$('#So4').attr('checked', true);
		}
	}
	
});

//Abschicken der Änderungen
$('#sendeZeit').click(function(){
		
		//Überprüfung ob Urlaubsmodus aktiviert ist
		if($('#Urlaubsmodus').is(':checked')){
			var urlaub = 1;	
			window.alert('Der Urlaubsmodus wurde aktiviert !');
			socket.emit('urlaub', urlaub);
		}
		else{
			urlaub = 0;
			//socket.emit('löscheUrlaub', 'true');
			socket.emit('urlaub', urlaub);
		}
				
		//Zeile 1 einlesen
				
		var Mo1 = 0;
		if($('#Mo1').is(':checked')){
			Mo1 = 1;
		}
		var Di1 = 0;
		if($('#Di1').is(':checked')){
			Di1 = 1;
		}
		var Mi1 = 0;
		if($('#Mi1').is(':checked')){
			Mi1 = 1;
		}
		var Do1 = 0;
		if($('#Do1').is(':checked')){
			Do1 = 1;
		}
		var Fr1 = 0;
		if($('#Fr1').is(':checked')){
			Fr1 = 1;
		}
		var Sa1 = 0;
		if($('#Sa1').is(':checked')){
			Sa1 = 1;
		}
		var So1 = 0;
		if($('#So1').is(':checked')){
			So1 = 1;
		}
			
		//Objekt für die erste Zeile ertellen
			zeile1 = {
				id				: 1,
				verb			: 1,
				anStd			: $('#timepicker1').data('timepicker').hour,
				anMin			: $('#timepicker1').data('timepicker').minute,
				ausStd		: $('#timepicker2').data('timepicker').hour,
				ausMin		: $('#timepicker2').data('timepicker').minute,
				Mo				: Mo1,
				Di				: Di1,
				Mi				: Mi1,
				Do				: Do1,
				Fr				: Fr1,
				Sa				: Sa1,
				So				: So1
			}
			socket.emit('löscheZeit','true');
			socket.emit('schreibeZeit1',zeile1);
			
		//Zeile 2 einlesen
			
		var Mo2 = 0;
		if($('#Mo2').is(':checked')){
			Mo2 = 1;
		}
		var Di2 = 0;
		if($('#Di2').is(':checked')){
			Di2 = 1;
		}
		var Mi2 = 0;
		if($('#Mi2').is(':checked')){
			Mi2 = 1;
		}
		var Do2 = 0;
		if($('#Do2').is(':checked')){
			Do2 = 1;
		}
		var Fr2 = 0;
		if($('#Fr2').is(':checked')){
			Fr2 = 1;
		}
		var Sa2 = 0;
		if($('#Sa2').is(':checked')){
			Sa2 = 1;
		}
		var So2 = 0;
		if($('#So2').is(':checked')){
			So2 = 1;
		}
		
		var anStd2 =$('#timepicker3').data('timepicker').hour;
		var anMin2 = $('#timepicker3').data('timepicker').minute;
		var ausStd2 = $('#timepicker4').data('timepicker').hour;
		var ausMin2 = $('#timepicker4').data('timepicker').minute;
		
		//Objekt für die zweite Zeile ertellen
			zeile2 = {
				id				: 2,
				verb			: 1,
				anStd			: anStd2,
				anMin			: anMin2,
				ausStd		: ausStd2,
				ausMin		: ausMin2,
				Mo				: Mo2,
				Di				: Di2,
				Mi				: Mi2,
				Do				: Do2,
				Fr				: Fr2,
				Sa				: Sa2,
				So				: So2
			}
			socket.emit('schreibeZeit2',zeile2);
		
		//Zeile 3 einlesen
			
		var Mo3 = 0;
		if($('#Mo3').is(':checked')){
			Mo3 = 1;
		}
		var Di3 = 0;
		if($('#Di3').is(':checked')){
			Di3 = 1;
		}
		var Mi3 = 0;
		if($('#Mi3').is(':checked')){
			Mi3 = 1;
		}
		var Do3 = 0;
		if($('#Do3').is(':checked')){
			Do3 = 1;
		}
		var Fr3 = 0;
		if($('#Fr3').is(':checked')){
			Fr3 = 1;
		}
		var Sa3 = 0;
		if($('#Sa3').is(':checked')){
			Sa3 = 1;
		}
		var So3 = 0;
		if($('#So3').is(':checked')){
			So3 = 1;
		}
		
		var anStd3 =$('#timepicker5').data('timepicker').hour;
		var anMin3= $('#timepicker5').data('timepicker').minute;
		var ausStd3 = $('#timepicker6').data('timepicker').hour;
		var ausMin3 = $('#timepicker6').data('timepicker').minute;
		
		//Objekt für die dritte Zeile ertellen
			zeile3 = {
				id				: 3,
				verb			: 2,
				anStd			: anStd3,
				anMin			: anMin3,
				ausStd		: ausStd3,
				ausMin		: ausMin3,
				Mo				: Mo3,
				Di				: Di3,
				Mi				: Mi3,
				Do				: Do3,
				Fr				: Fr3,
				Sa				: Sa3,
				So				: So3
			}
			
			socket.emit('schreibeZeit3',zeile3);
			
			//Zeile 4 einlesen
			
		var Mo4 = 0;
		if($('#Mo4').is(':checked')){
			Mo4 = 1;
		}
		var Di4 = 0;
		if($('#Di4').is(':checked')){
			Di4 = 1;
		}
		var Mi4 = 0;
		if($('#Mi4').is(':checked')){
			Mi4 = 1;
		}
		var Do4 = 0;
		if($('#Do4').is(':checked')){
			Do4 = 1;
		}
		var Fr4 = 0;
		if($('#Fr4').is(':checked')){
			Fr4 = 1;
		}
		var Sa4 = 0;
		if($('#Sa4').is(':checked')){
			Sa4 = 1;
		}
		var So4 = 0;
		if($('#So4').is(':checked')){
			So4 = 1;
		}
		
		var anStd4 =$('#timepicker7').data('timepicker').hour;
		var anMin4 = $('#timepicker7').data('timepicker').minute;
		var ausStd4 = $('#timepicker8').data('timepicker').hour;
		var ausMin4 = $('#timepicker8').data('timepicker').minute;
		
		//Objekt für die vierte Zeile ertellen
			zeile4 = {
				id				: 4,
				verb			: 2,
				anStd			: anStd4,
				anMin			: anMin4,
				ausStd		: ausStd4,
				ausMin		: ausMin4,
				Mo				: Mo4,
				Di				: Di4,
				Mi				: Mi4,
				Do				: Do4,
				Fr				: Fr4,
				Sa				: Sa4,
				So				: So4
			}
			socket.emit('schreibeZeit4',zeile4);
		

		});
	});




