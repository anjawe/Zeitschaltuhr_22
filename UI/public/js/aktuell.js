/// socket-Events der Startseite

/// Projekt:	Zeitschaltuhr
/// Hersteller:	Timecode 22 GmbH

/// Erstellt von:	Lena Kopp
/// Datum:		    2015.12.19
/// Revision:       ~Name
/// Datum:          JJJJ.MM.DD
/// Qualitätscheck:	~Name
/// Datum:		    JJJJ.MM.DD

$(document).ready(function()
{
//Verbraucher 1
    socket.on('naechst1', function(naechst1)
    {
    		if(naechst1.Std < 10 && naechst1.Min < 10)
    		{
        $('#naechst1').text('0' + naechst1.Std +':' + '0' + naechst1.Min);
    			return;
    		}
    		if(naechst1.Std < 10)
    		{ 
    			$('#naechst1').text('0' + naechst1.Std +':' + naechst1.Min);
    			return;
    		}
    		if(naechst1.Min < 10)
    		{ 
    			$('#naechst1').text(naechst1.Std +':'+ '0' + naechst1.Min);
    			return;
    		}
    		
        $('#naechst1').text(naechst1.Std +':'+ naechst1.Min);
    });

    socket.on('verb1Status', function(status1)
    {
        if(status1 == 0)
        {
            $('#zustand1').removeClass('label label-success');
            $('#zustand1').addClass('label label-danger');

        }

        if(status1 == 1)
        {
            $('#zustand1').removeClass('label label-danger');
            $('#zustand1').addClass('label label-success');
        }
    });
//Verbraucher 2
    socket.on('naechst2', function(naechst2)
    {
    		if(naechst2.Std < 10 && naechst2.Min < 10)
    		{ 
    			$('#naechst2').text('0' + naechst2.Std +':' + '0' + naechst2.Min);
    			return;
    		}
    		if(naechst2.Std < 10)
    		{ 
    			$('#naechst2').text('0' + naechst2.Std +':' + naechst2.Min);
    			return;
    		}
    		if(naechst2.Min < 10)
    		{ 
    			$('#naechst2').text(naechst2.Std +':'+ '0' + naechst2.Min);
    			return;
    		}
    		
        $('#naechst2').text(naechst2.Std +':'+ naechst2.Min);

    });

    socket.on('verb2Status', function(status2)
    {
        if(status2 == 0)
        {
            $('#zustand2').removeClass('label label-success');
            $('#zustand2').addClass('label label-danger');
        }

        if(status2 == 1)
        {
            $('#zustand2').removeClass('label label-danger');
            $('#zustand2').addClass('label label-success');
        }
    });




});
