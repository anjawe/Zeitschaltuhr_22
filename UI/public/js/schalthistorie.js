/// Ausgeben der Schalthistorie

/// Projekt:	Zeitschaltuhr
/// Hersteller:	Timecode 22 GmbH

/// Erstellt von:	Lena Kopp
/// Datum:		    2015.12.19
/// Revision:       ~Name
/// Datum:          JJJJ.MM.DD
/// Qualitätscheck:	~Name
/// Datum:		    JJJJ.MM.DD



$(document).ready(function()
{

    socket.emit('leseHistorie','true');
    socket.on('historie',function(eintrag)
    {
    		// http://stackoverflow.com/questions/26446094/convert-mysql-timestamp-to-local-time-in-javascript
				var zeit = eintrag.zeitpunkt.split(/[- : T .]/);
				var d = new Date(zeit[0], zeit[1]-1, zeit[2], zeit[3], zeit[4], zeit[5]);
				var localZeit = new Date(d.getTime() - ( d.getTimezoneOffset() * 60000 ));
				var format = localZeit.toString().split(" ");
				
        $("#historie").append(
            "<tr>" +
            "<td>" +"Verbraucher "+ eintrag.verb + "</td>" +
            "<td>" + eintrag.ereignis + "</td>" +
            "<td>" + format[0] + " " + format[2] + ". " + format[1] + " " + format[3]  + " " + format[4] + "</td>" +
            "</tr>"
        );
    });

});

