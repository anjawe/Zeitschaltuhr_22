/// socket-Events zur Kommunikation zwischen Server und Client

/// Projekt:	Zeitschaltuhr
/// Hersteller:	Timecode 22 GmbH

/// Erstellt von:	Lena Kopp
/// Datum:		    2016.01.07
/// Revision:	    ~Name
/// Datum:		    JJJJ.MM.DD
/// Qualitätscheck:	~Name
/// Datum:		    JJJJ.MM.DD



module.exports = function(io)
{

    var moment = require('moment');
    var fs = require('fs');
    var path = require('path');
    var mysql = require('mysql');
    var query;

//mySQL-Datenbank einbinden
    var connection= mysql.createConnection(
    {
			host				:	'localhost',
			user				:	'root',
			password	    	:	'123456',
								database	    	:	'zeitschaltuhr',

    });

//mit der Datenbank verbinden
    connection.connect();

//socket-Funktionen

    io.on('connection', function(socket)
    {


        /***************************************/
        /*Datum und Zeit */
        /***************************************/
        setInterval(function()
        {
            var jetzt = moment();
            jetzt.locale('de');
            var datum = jetzt.format('dddd DD.MM.YYYY');
            var zeit = jetzt.format('HH:mm');
            //Datum und Uhrzeit werden zum Client gesendet
            socket.emit('datum', {'datum':datum , 'zeit':zeit });



        },100);

        /***************************************/
        /* Status und nächste Schaltzeit wird aus der Datenbank gelesen */
        /***************************************/
        setInterval(function()
        {

            var jetzt = moment();
            jetzt.locale('de');

            //Ermittlung der nächsten Schaltzeit des Tages des Verbrauchers 1
            connection.query("select * from schaltzeiten where verbraucher=1 and "+jetzt.format("dd")+"=1 and hh_an > "+jetzt.format("HH")+"", function(err,result)
            {
                if(err)
                {
                    console.error(err);
                    return;
                }
                //console.error(result);
                if(result.length > 0)
                {
                    var nachstStd =
                    {
											Std	:	result[0].hh_an,
											Min	:	result[0].mm_an
                    }
                    socket.emit('naechst1',nachstStd);
                }
                else
                {
                    connection.query("select * from schaltzeiten where verbraucher=1 and "+jetzt.format("dd")+"=1 and hh_an = "+jetzt.format("HH")+" and mm_an > "+jetzt.format("mm")+"", function(err,result)
                    {
                        if(err)
                        {
                            console.error(err);
                            return;
                        }
                        if(result.length > 0)
                        {
                            var nachstStd =
                            {
															Std	:	result[0].hh_an,
															Min	: result[0].mm_an
                            }
                            socket.emit('naechst1',nachstStd);
                        }
                        else
                        {
                            connection.query("select * from schaltzeiten where verbraucher=1 and "+jetzt.format("dd")+"=1 and hh_aus > "+jetzt.format("HH")+"", function(err,result)
                            {
                                if(err)
                                {
                                    console.error(err);
                                    return;
                                }
                                if(result.length > 0)
                                {
                                    var nachstStd =
                                    {
																			Std	:	result[0].hh_aus,
																			Min	: result[0].mm_aus
                                    }
                                    socket.emit('naechst1',nachstStd);
                                }
                                else
                                {
                                    connection.query("select * from schaltzeiten where verbraucher=1 and "+jetzt.format("dd")+"=1 and hh_aus = "+jetzt.format("HH")+" and mm_aus > "+jetzt.format("mm")+"", function(err,result)
                                    {
                                        if(err)
                                        {
                                            console.error(err);
                                            return;
                                        }
                                        //console.error(result);
                                        if(result.length > 0)
                                        {
                                            var nachstStd =
                                            {
																							Std	:	result[0].hh_aus,
																							Min	:	result[0].mm_aus
                                            }
                                            socket.emit('naechst1',nachstStd);
                                        }
                                    });

                                }
                            });
                        }
                    });
                }
            });

            //Ermittlung der nächsten Schaltzeit des Tages des Verbrauchers 2
            connection.query("select * from schaltzeiten where verbraucher=2 and "+jetzt.format("dd")+"=1 and hh_an > "+jetzt.format("HH")+"", function(err,result)
            {
                if(err)
                {
                    console.error(err);
                    return;
                }
                if(result.length > 0)
                {
                    var nachstStd =
                    {
											Std	:	result[0].hh_an,
											Min	:	result[0].mm_an
                    }
                    socket.emit('naechst2',nachstStd);
                }
                else
                {
                    connection.query("select * from schaltzeiten where verbraucher=2 and "+jetzt.format("dd")+"=1 and hh_an = "+jetzt.format("HH")+" and mm_an > "+jetzt.format("mm")+"", function(err,result)
                    {
                        if(err)
                        {
                            console.error(err);
                            return;
                        }
                        //console.error(result);
                        if(result.length > 0)
                        {
                            var nachstStd =
                            {
															Std	:	result[0].hh_an,
															Min	:	result[0].mm_an
                            }
                            socket.emit('naechst2',nachstStd);
                        }
                        else
                        {
                            connection.query("select * from schaltzeiten where verbraucher=2 and "+jetzt.format("dd")+"=1 and hh_aus > "+jetzt.format("HH")+"", function(err,result)
                            {
                                if(err)
                                {
                                    console.error(err);
                                    return;
                                }
                                if(result.length > 0)
                                {
                                    var nachstStd =
                                    {
																			Std	:	result[0].hh_aus,
																			Min	:	result[0].mm_aus
                                    }
                                    socket.emit('naechst2',nachstStd);
                                }
                                else
                                {
                                    connection.query("select * from schaltzeiten where verbraucher=2 and "+jetzt.format("dd")+"=1 and hh_aus = "+jetzt.format("HH")+" and mm_aus > "+jetzt.format("mm")+"", function(err,result)
                                    {
                                        if(err)
                                        {
                                            console.error(err);
                                            return;
                                        }
                                        //console.error(result);
                                        if(result.length > 0)
                                        {
                                            var nachstStd =
                                            {
																							Std	:	result[0].hh_aus,
																							Min	:	result[0].mm_aus
                                            }
                                            socket.emit('naechst2',nachstStd);
                                        }
                                    });

                                }
                            });

                        }
                    });
                }
            });


        },1000);

        setInterval(function()
        {
            //aktueller Zustand des Verbraucher 1 auslesen
            connection.query("select * from verbraucherzustaende where verbraucher=1", function(err,result)
            {
                if(err)
                {
                    console.error(err);
                    return;
                }
                //console.error(result);
                socket.emit('verb1Status', result[0].zustand);
            });
            //aktueller Zustand des Verbraucher 2 auslesen
            connection.query("select * from verbraucherzustaende  where verbraucher=2", function(err,result)
            {
                if(err)
                {
                    console.error(err);
                    return;
                }
                //console.error(result);
                socket.emit('verb2Status', result[0].zustand);
            });
        },100);

        /***************************************/
        /*Tastermodus wird in der Datenbank abgelegt */
        /***************************************/

        //löscht den Inhalt der Tastermodus Tabelle
        socket.on('löscheTaster', function(bool)
        {
            if(bool == 'true')
            {
                connection.query("TRUNCATE TABLE tastermodus", function(err,result)
                {
                    if(err)
                    {
                        console.error(err);
                        return;
                    }
                    //console.error(result);
                });
            }

        });

        //Abspeichern des Tastermodus
        socket.on('tastermodus',function(taster)
        {
            connection.query("insert into tastermodus (verbraucher, modus) values ('"+taster.verb+"', '"+taster.modus+"')", function(err,result)
            {
                if(err)
                {
                    console.error(err);
                    return;
                }

            });
        });

        //schreiben des Tastermodus
        socket.on('leseTastermodus',function(aufruf)
        {
            if(aufruf == 'true')
            {
                connection.query("select * from tastermodus where verbraucher = 1", function(err,result)
                {
                    if(err)
                    {
                        console.error(err);
                        return;
                    }
                    socket.emit('taster1',result[0].modus);

                });
                connection.query("select * from tastermodus where verbraucher = 2", function(err,result)
                {
                    if(err)
                    {
                        console.error(err);
                        return;
                    }
                    socket.emit('taster2',result[0].modus);
                });
            }
        });
        /***************************************/
        /* Urlaubsmodus wird in der Datenbank abgelegt */
        /***************************************/

        socket.on('urlaub',function(urlaub)
        {
            connection.query("update urlaubsmodus set status='"+urlaub+"'", function(err,result)
            {
                if(err)
                {
                    console.error(err);
                    return;
                }
                ////console.error(result);
            });
        });

        /***************************************/
        /* Zeitplan wird in der Datenbank abgelegt */
        /***************************************/

        //schreibt die Schaltzeiten in die Datenbank
        socket.on('schreibeZeit1',function(zeile)
        {
            connection.query("update schaltzeiten set verbraucher='"+zeile.verb+"', hh_an = '"+zeile.anStd+"', mm_an='"+zeile.anMin+"', hh_aus='"+zeile.ausStd+"', mm_aus='"+zeile.ausMin+"', Mo='"+zeile.Mo+"', Di='"+zeile.Di+"', Mi='"+zeile.Mi+"', Do='"+zeile.Do+"', Fr='"+zeile.Fr+"', Sa= '"+zeile.Sa+"', So='"+zeile.So+"' where id='"+zeile.id+"'", function(err,result)
            {
                if(err)
                {
                    console.error(err);
                    return;
                }
                //console.error(result);
            });
        });

        socket.on('schreibeZeit2',function(zeile)
        {
            connection.query("update schaltzeiten set verbraucher='"+zeile.verb+"', hh_an = '"+zeile.anStd+"', mm_an='"+zeile.anMin+"', hh_aus='"+zeile.ausStd+"', mm_aus='"+zeile.ausMin+"', Mo='"+zeile.Mo+"', Di='"+zeile.Di+"', Mi='"+zeile.Mi+"', Do='"+zeile.Do+"', Fr='"+zeile.Fr+"', Sa= '"+zeile.Sa+"', So='"+zeile.So+"' where id='"+zeile.id+"'", function(err,result)
            {
                if(err)
                {
                    console.error(err);
                    return;
                }
                //console.error(result);
            });
        });
        socket.on('schreibeZeit3',function(zeile)
        {
            //console.log(zeile);
            connection.query("update schaltzeiten set verbraucher='"+zeile.verb+"', hh_an = '"+zeile.anStd+"', mm_an='"+zeile.anMin+"', hh_aus='"+zeile.ausStd+"', mm_aus='"+zeile.ausMin+"', Mo='"+zeile.Mo+"', Di='"+zeile.Di+"', Mi='"+zeile.Mi+"', Do='"+zeile.Do+"', Fr='"+zeile.Fr+"', Sa= '"+zeile.Sa+"', So='"+zeile.So+"' where id='"+zeile.id+"'", function(err,result)
            {
                if(err)
                {
                    console.error(err);
                    return;
                }
                //console.error(result);
            });
        });

        socket.on('schreibeZeit4',function(zeile4)
        {
            connection.query("update schaltzeiten set verbraucher='"+zeile4.verb+"', hh_an = '"+zeile4.anStd+"', mm_an='"+zeile4.anMin+"', hh_aus='"+zeile4.ausStd+"', mm_aus='"+zeile4.ausMin+"', Mo='"+zeile4.Mo+"', Di='"+zeile4.Di+"', Mi='"+zeile4.Mi+"', Do='"+zeile4.Do+"', Fr='"+zeile4.Fr+"', Sa= '"+zeile4.Sa+"', So='"+zeile4.So+"' where id='"+zeile4.id+"'", function(err,result)
            {
                if(err)
                {
                    console.error(err);
                    return;
                }
                //console.error(result);
            });
        });

        /***************************************/
        /* Zeitplan wird aus der Datenbank gelesen */
        /***************************************/


        socket.on('leseZeit',function(bool)
        {
            if(bool == 'true')
            {

                connection.query("select * from urlaubsmodus", function(err,result)
                {
                    if(err)
                    {
                        console.error(err);
                        return;
                    }
                    socket.emit('urlaubsmodus',result[0]);
                    //console.log(result);
                });

                connection.query("select * from schaltzeiten", function(err,result)
                {
                    if(err)
                    {
                        console.error(err);
                        return;
                    }

                    var reihen;

                    for(reihen = 0; reihen < result.length; reihen++)
                    {
                        var zeile =
                        {
													id  		: result[reihen].id,
													anStd 	:	result[reihen].hh_an,
													anMin 	:	result[reihen].mm_an,
													ausStd	:	result[reihen].hh_aus,
													ausMin  :	result[reihen].mm_aus,
													Mo  		:	result[reihen].Mo,
													Di  		:	result[reihen].Di,
													Mi  		:	result[reihen].Mi,
													Do  		:	result[reihen].Do,
													Fr  		:	result[reihen].Fr,
													Sa  		:	result[reihen].Sa,
													So  		:	result[reihen].So
                        }
                        //console.error(zeile);
                        socket.emit('zeile',zeile);


                    }
                });
            }
        });

        /***************************************/
        /*Schalthistorie aus der Datenbank lesen */
        /***************************************/

        socket.on('leseHistorie',function(lese)
        {
            if(lese == 'true')
            {
                connection.query("select * from schalthistorie", function(err,result)
                {
                    if(err)
                    {
                        console.error(err);
                        return;
                    }
                    ////console.error(result);
                    var reihe;

                    for(reihe = 0; reihe < result.length; reihe++)
                    {
                        var verbraucher = result[reihe].verbraucher;

                        var status;
                        if(result[reihe].ereignis == 1)
                        {
                            status = "an";
                        }
                        else
                        {
                            status = "aus";
                        }
                        var zeitpunkt = result[reihe].zeitpunkt;



                        var eintrag =
                        {
													verb			:	verbraucher,
													ereignis	:	status,
													zeitpunkt	:	zeitpunkt
                        }

                        socket.emit('historie',eintrag);

                    }
                });
            }
        });


    });


}
