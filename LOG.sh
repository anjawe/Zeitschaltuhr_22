#!/bin/bash


git log autostart.sh > log/autostart.sh+.log
git log build.sh > log/build.sh+.log
git log datenbank_sql_befehle.sql > log/datenbank_sql_befehle.sql+.log
git log npmstart.sh > log/npmstart.sh+.log
git log UI > log/UI+.log
git log /Quellcode/DBKommunikation.cpp > log/DBKommunikation.cpp+.log
git log /Quellcode/DBKommunikation.h > log/DBKommunikation.h+.log
git log /Quellcode/main.cpp > log/main.cpp+.log
git log /Quellcode/Systemuhr.cpp > log/Systemuhr.cpp+.log
git log /Quellcode/Systemuhr.h > log/Systemuhr.h+.log
git log /Quellcode/Urlaubsmodus.cpp > log/Urlaubsmodus.cpp+.log
git log /Quellcode/Urlaubsmodus.h > log/Urlaubsmodus.h+.log
git log /Quellcode/Verbraucher.h > log/Verbraucher.h+.log
git log /Quellcode/Verbraucher.cpp > log/Verbraucher.cpp+.log
git log /Quellcode/ZSU_22.cbp > log/ZSU_22.cbp+.log
